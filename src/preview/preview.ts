import { setAny, IDefinition } from "tripetto-collector";
import "./preview.scss";

/**
 * Runs the collector in preview mode.
 * @param pkg Reference to the collector package.
 * @param props Specifies the properties for the collector.
 */
export async function preview<Style, Overrides>(
    pkg: {
        run: ({}) => Promise<{
            requestPreview: (id: string) => void;
            resize: () => void;
        }>;
    },
    props: {
        readonly definition?: IDefinition;
        readonly view: "preview" | "test";
        readonly style?: Style;
        readonly overrides?: Overrides;
    }
): Promise<void> {
    const collector = await pkg.run({
        element: document.body,
        view: props.view,
        usage: "preview",
        definition: props.definition,
        style: props.style,
        overrides: props.overrides,
        onEditRequest: (id: string) => {
            window.parent.postMessage(
                {
                    type: "editRequest",
                    id
                },
                window.location.origin
            );
        }
    });

    window.addEventListener("message", (e: MessageEvent) => {
        if (e.data) {
            if (e.data.name === "requestPreview") {
                collector.requestPreview(e.data.id);
            } else if (e.data.name) {
                setAny(collector, e.data.name, e.data.value);
            }
        }
    });

    window.addEventListener("load", () => window.parent.postMessage("ready", window.location.origin));
}
