<html>
  <body>
    <h1 style="font-size: 24px; color: #2c405a; line-height: 32px; margin: 0;">
        <?php esc_html_e('New submission', 'tripetto'); ?>
    </h1>
    <p>
    <?php esc_html_e(
        'A new submission was received for form',
        'tripetto'
    ); ?> {{name}} (<a href="{{entryUrl}}"><?php esc_html_e(
     'Manage entry',
     'tripetto'
 ); ?></a>).
    </p>
    
    {{data}}

    {{footer}}
    
  </body>
</html>
