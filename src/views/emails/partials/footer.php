<table
  cellspacing="0"
  cellpadding="0"
  style="width: 100%; border-top: 1px solid #e5eef5; padding-top: 16px; padding-bottom: 16px;"
>
  <tr>
    <td style="padding-right: 16px;">
      <a href="https://tripetto.com" target="_blank">
        <img
          src="{{url}}/assets/mail-logo-tripetto.png"
          width="24"
          height="24"
          style="border: 0;"
        />
      </a>
    </td>
    <td style="width: 100%;">
      <div
        style="display: block; font-size: 12px; color: #3f536e; line-height: 18px;"
      >
        Sent to
        <a
          href="#"
          style="color: #3f536e; text-decoration: none;font-weight: bold;"
          >{{recipient}}</a
        >
        by
        <a
          href="https://tripetto.com"
          target="_blank"
          style="color: #3f536e; text-decoration: underline;"
          >Tripetto</a
        >
      </div>
      <div
        style="display: block; font-size: 12px; color: #8dabc4; line-height: 18px;"
      >
        Get conversational!
      </div>
    </td>
  </tr>
</table>
