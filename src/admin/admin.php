<?php
include 'forms/forms.php';

class Admin
{
    static function scripts()
    {
        $plugin_url = Helpers::plugin_url();

        wp_register_style(
            'wp-tripetto-admin',
            $plugin_url . '/css/wp-tripetto-admin.css',
            array(),
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"]
        );

        wp_register_script(
            'wp-tripetto-admin',
            $plugin_url . '/scripts/wp-tripetto-admin.js',
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );

        add_thickbox();
    }

    static function menu()
    {
        add_menu_page(
            __('Tripetto', 'tripetto'),
            __('Tripetto', 'tripetto'),
            'manage_options',
            'tripetto',
            'Admin::page',
            'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNTAwIiBoZWlnaHQ9IjUwMCIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgNTAwIDUwMCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+PG1ldGFkYXRhPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGRlZnM+PGNsaXBQYXRoIGlkPSJjbGlwUGF0aDI4Ij48cGF0aCBkPSJtMCA2MTJoNzkydi02MTJoLTc5MnoiLz48L2NsaXBQYXRoPjwvZGVmcz48ZyB0cmFuc2Zvcm09Im1hdHJpeCgxLjMzMzMgMCAwIC0xLjMzMzMgLTI5Mi4zMyA5NzEuODIpIj48ZyB0cmFuc2Zvcm09Im1hdHJpeCgyLjE3NDggMCAwIDIuMTYyOCAtNC40ODY5IC0xMjAuNDUpIj48ZyBjbGlwLXBhdGg9InVybCgjY2xpcFBhdGgyOCkiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIzMC44IDM0NC41MykiPjxwYXRoIGQ9Im0wIDAgNDEuNzEyIDI0LjA4My0yMC44NTYgMTIuMDQxLTIwLjg1NyAxMi4wNDF6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTg5LjA5IDMyMC40NSkiPjxwYXRoIGQ9Ik0gMCwwIDQxLjcxMywyNC4wODMgMCw0OC4xNjYgWiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIwOS45NCAzNTYuNTcpIj48cGF0aCBkPSJtMCAwIDIwLjg1Ni0xMi4wNDItMWUtMyA0OC4xNjUtMjAuODU1LTEyLjA0MS0yMC44NTYtMTIuMDR6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTg5LjA5IDMyMC40NSkiPjxwYXRoIGQ9Im0wIDB2NDguMTY1bC00MS43MTEtMjQuMDgyeiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE0Ny4zNyAzNDQuNTMpIj48cGF0aCBkPSJtMCAwIDQxLjcxMiAyNC4wODMtNDEuNzEzIDI0LjA4MnoiIGZpbGw9IiNhMGE1YWEiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxNDcuMzcgMzQ0LjUzKSI+PHBhdGggZD0ibTAgMC0xZS0zIDI1Ljc3N3YyMi4zODhsLTQxLjcxMi0yNC4wODJ6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjc1LjMgMzYzLjgpIj48cGF0aCBkPSJtMCAwLTQxLjcxMi0yNC4wODMgNDEuNzEyLTI0LjA4M3YyNC4wODN6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjc1LjMgMzE1LjY0KSI+PHBhdGggZD0ibTAgMC00MS43MTEgMjQuMDgzdi00OC4xNjVsMjAuODU1IDEyLjA0eiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIzMy41OSAzMTUuNjQpIj48cGF0aCBkPSJNIDAsMCBWIDI0LjA4MiBMIC00MS43MTMsMCAwLC0yNC4wODMgWiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE5MS44OCAzMTUuNjQpIj48cGF0aCBkPSJtMCAwIDFlLTMgLTQ4LjE2NSA0MS43MTIgMjQuMDgzLTIwLjg1NyAxMi4wNDF6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjEyLjczIDI3OS41MSkiPjxwYXRoIGQ9Im0wIDAtMjAuODU1LTEyLjA0MSA0MS43MTEtMjQuMDgzdjQ4LjE2NnoiIGZpbGw9IiNhMGE1YWEiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyMzMuNTkgMjQzLjM5KSI+PHBhdGggZD0ibTAgMC0yMi41ODEgMTMuMDM3LTE5LjEzIDExLjA0Ni0xZS0zIC00OC4xNjYgMjAuODU2IDEyLjA0MnoiIGZpbGw9IiNhMGE1YWEiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxNDQuNTkgMzM5LjczKSI+PHBhdGggZD0ibTAgMC00MS43MTMgMjQuMDgzdi00OC4xNjZ6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTQ0LjU5IDMzOS43MykiPjxwYXRoIGQ9Im0wIDAtNDEuNzEzLTI0LjA4MyAyMC44NTYtMTIuMDQyIDIwLjg1Ny0xMi4wNDF6IiBmaWxsPSIjYTBhNWFhIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTQ0LjU5IDMzOS43MykiPjxwYXRoIGQ9Im0wIDAtMWUtMyAtMjQuMDgzIDFlLTMgLTI0LjA4MyA0MS43MTEgMjQuMDgzeiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE2NS40NCAzMDMuNikiPjxwYXRoIGQ9Im0wIDAtMjAuODU2LTEyLjA0MSA0MS43MTMtMjQuMDgyLTFlLTMgNDguMTY0eiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE4Ni4zIDIxOS4zMSkiPjxwYXRoIGQ9Im0wIDAgMWUtMyA0OC4xNjYtNDEuNzEzLTI0LjA4NCAyMC44NTYtMTIuMDQxeiIgZmlsbD0iI2EwYTVhYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE0NC41OSAyNDMuNCkiPjxwYXRoIGQ9Ik0gMCwwIDE5LjUwNiwxMS4yNjMgNDEuNzEzLDI0LjA4NCAyMC44NTYsMzYuMTI0IDAsNDguMTY2IFYgMjQuMDgzIFoiIGZpbGw9IiNhMGE1YWEiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvZz48L2c+PC9nPjwvZz48L3N2Zz4K'
        );

        AdminForms::menu();
    }

    static function page()
    {
        global $wpdb;
        $table_forms = $wpdb->prefix . 'tripetto_forms';
        $table_entries = $wpdb->prefix . 'tripetto_entries';

        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_forms");
        $total_entries = $wpdb->get_var("SELECT COUNT(id) FROM $table_entries");

        wp_enqueue_style('wp-tripetto-admin');

        // prettier-ignore
        // empty h2 below is for controlling admin notice position
        echo '
            <div class="wrap tripetto-wrap">
            <h2></h2>
                <div id="welcome-panel" class="welcome-panel">
                    <div class="welcome-panel-content">
                        <img src="' . Helpers::plugin_url() . '/images/tripetto.svg" style="height:50px;widht:50px;" />
                        <h2>' . __('Welcome to Tripetto!', 'tripetto') . '</h2>
                        <p class="about-description">' . __('Easily create smart forms and surveys. From a simple contact form to an advanced survey with lots of logic.', 'tripetto') . '</p>
                        ' . (License::isInPremiumMode() ? '' : '
                        <div class="panel-feedback">
                            <h3>🎁 Get a FREE premium license for giving us your feedback</h3>
                            <p>We are curious about what you think of Tripetto. We’ll give you a <strong>FREE one year premium license</strong> if you share your honest feedback with us.</p>
                            <a href="https://tripetto.app/collect/VRY9TNZBVD" target="_blank">👉 Click here to share your feedback</a>
                            <p>We will send you a premium license by email shortly after you submit your feedback. Many thanks in advance for helping us get better!</p>
                        </div>
                        ') .
                        '<div class="welcome-panel-column-container">
                            <div class="welcome-panel-column">
                                <h3>' . __('Get Started', 'tripetto') . '</h3>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fas fa-cubes fa-fw"></i></span>' . __('Create your form using the visual form builder', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fas fa-search fa-fw"></i></span>' . __('See a realtime preview of your form, on different devices', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fas fa-sliders-h fa-fw"></i></span>' . __('Style your form and remove branding', 'tripetto') . '<span class="premium">' . __('Premium', 'tripetto') . '</span></li>
                                </ul>

                            </div>
                            <div class="welcome-panel-column">
                                <h3>' . __('Publish your form', 'tripetto') . '</h3>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fas fa-share-square fa-fw"></i></span>' . __('Publish your form using the Tripetto shortcode <code>[tripetto id="<span style="color:#0073aa">#</span>"]', 'tripetto') . '</code></li>
                                    <li><span class="fa-li"><i class="fas fa-book fa-fw"></i></span>' . __('See our <a href="https://tripetto.com/wordpress/get-started/" target="_blank">Get Started page</a> for more information about using the shortcode and its parameters', 'tripetto') . '</li>
                                </ul>
                            </div>
                            <div class="welcome-panel-column">
                                <h3>' . __('Collect responses and get notified', 'tripetto') . '</h3>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fas fa-poll fa-fw"></i></span>' . __('View responses and export to CSV', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fas fa-bell fa-fw"></i></span>' . __('Get notified of new responses in Slack', 'tripetto') . '<span class="premium">' . __('Premium', 'tripetto') . '</span></li>
                                    <li><span class="fa-li"><i class="fas fa-network-wired fa-fw"></i></span>' . __('Connect to 1.500+ apps with Zapier', 'tripetto') . '<span class="premium">' . __('Premium', 'tripetto') . '</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="welcome-panel-column-container welcome-panel-footer">
                            <a class="button button-primary button-hero load-customize" href="?page=tripetto-forms">' . __('View All Forms', 'tripetto') . '</a>
                            <a class="button button-secondary button-hero load-customize" href="?page=tripetto-forms&action=create">' . __('Create New Form', 'tripetto') . '</a>
                            <p>Or visit our <a href="https://tripetto.com/wordpress/get-started/" target="_blank">Get Started page</a> for <a href="https://tripetto.com/wordpress/get-started/#instructions" target="_blank">instructions</a>, <a href="https://tripetto.com/wordpress/get-started/#templates" target="_blank">templates</a> and <a href="https://tripetto.com/wordpress/get-started/#tutorials" target="_blank">tutorials</a>.</p>
                        </div>
                    </div>
                </div>
                <div id="dashboard-widgets-wrap">
                    <div id="dashboard-widgets" class="metabox-holder">
                        <div id="postbox-container-1" class="postbox-container">
                            <div id="normal-sortables" class="meta-box-sortables">
                                <div class="postbox">
                                    <h2 class="hndle"><i class="fas fa-chart-line fa-fw"></i>' . __('Statistics', 'tripetto') . '</h2>
                                    <div class="inside" style="height:90px;">
                                        <div class="main">
                                            <ul class="sides">
                                                <li class="statistics-count">
                                                    <p>' . __('Total forms', 'tripetto') . '</p>
                                                    <span>' . $total_items . '</span>
                                                </li>
                                                <li class="statistics-count">
                                                    <p>' . __('Total entries', 'tripetto') . '</p>
                                                    <span>' . $total_entries . '</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="postbox">
                                    <h2 class="hndle"><i class="fas fa-life-ring fa-fw"></i>' . __('Support', 'tripetto') . '</h2>
                                    <div class="inside" style="height:137px;">
                                        <div class="main">
                                            <ul class="sides sides-single">
                                                <li>
                                                    <h3>Looking for instructions, templates, or tutorials?</h3>
                                                    <a href="https://tripetto.com/wordpress/get-started/" target="_blank">
                                                        <i class="fas fa-book fa-fw"></i>' . __('Visit our Get Started page', 'tripetto') . '
                                                    </a>
                                                </li>
                                            </ul>
                                            <ul class="sides">
                                                <li>
                                                    <h3>Got a question?</h3>
                                                    <a href="https://tripetto.com/contact/" target="_blank">
                                                        <i class="fas fa-question-circle fa-fw"></i>' . __('Ask for support', 'tripetto') . '
                                                    </a>
                                                </li>
                                                <li>
                                                    <h3>Found a bug?</h3>
                                                    <a href="https://gitlab.com/tripetto/wordpress/issues" target="_blank">
                                                        <i class="fas fa-bug fa-fw"></i>' . __('Submit an issue', 'tripetto') . '
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="postbox-container-2" class="postbox-container">
                            <div class="meta-box-sortables">
                                <div class="postbox">
                                    <h2 class="hndle"><i class="fas fa-star fa-fw"></i>' . __('Premium features', 'tripetto') . '</h2>
                                    <div class="inside" style="height:320px;">
                                        <div class="main">
                                            <p><strong>Always free: One form with all premium features.</strong></p>
                                            <h3 class="premium">Upgrade to premium and unlock the following extras for all your forms:</h3>
                                            <ul class="fa-ul premium">
                                                <li><span class="fa-li"><i class="fas fa-sliders-h fa-fw"></i></span>' . __('Style your form and remove branding', 'tripetto') . '</li>
                                                <li><span class="fa-li"><i class="fas fa-bell fa-fw"></i></span>' . __('Get notified of new responses in Slack', 'tripetto') . '</li>
                                                <li><span class="fa-li"><i class="fas fa-network-wired fa-fw"></i></span>' . __('Connect to 1.500+ apps with Zapier', 'tripetto') . '</li>
                                            </ul>
                                            ' . (t_freemius()->is_not_paying() ? ('<a class="button button-primary button-hero load-customize" href="' . t_freemius()->get_upgrade_url() . '">' . __('Upgrade To Premium', 'tripetto') . '</a>') : '<p class="upgraded"><i class="fas fa-check-circle"></i><strong>Thanks for upgrading!</strong></p>') . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="postbox-container-3" class="postbox-container">
                            <div class="meta-box-sortables">
                                <div class="postbox">
                                    <h2 class="hndle"><i class="fab fa-twitter fa-fw"></i>' . __('Stay tuned - Twitter', 'tripetto') . '</h2>
                                    <a class="twitter-timeline" data-width="100%" data-height="343" data-theme="light" data-chrome="noheader,nofooter,noborders" href="https://twitter.com/tripetto?ref_src=twsrc%5Etfw">Tweets by tripetto</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';
    }

    static function register($plugin)
    {
        if (is_admin()) {
            add_action('admin_enqueue_scripts', 'Admin::scripts');
            add_action('admin_menu', 'Admin::menu');

            AdminForms::register($plugin);
        }
    }
}
?>
