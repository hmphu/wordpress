<?php

// Extending default WP List Table Tripetto Class

class AdminEntriesList extends WP_List_Table_Tripetto
{
    /**
     * Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     */
    public function __construct()
    {
        parent::__construct(array(
            'singular' => __('entry', 'tripetto'),
            'plural' => __('entries', 'tripetto'),
            'ajax' => false
        ));
    }

    /**
     * This is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name)
    {
        return $esc_html($item[$column_name]);
    }

    /**
     * Render checkbox column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="entry_id[]" value="%s" />',
            $item['id']
        );
    }

    /**
     * Render Entry ID column with actions
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_id($item)
    {
        $actions = array(
            'view' => sprintf(
                '<a href="?page=%s&action=view&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('View Entry', 'tripetto')
            ),
            'delete' => sprintf(
                '<a href="javascript:;" onclick="WPTripetto.showModal(\'%s\',\'%s\',\'%s\',\'%s\',\'?page=%s&action=entries&id=%s&entry_id=%s\',400,210);">%s</a>',
                __('Confirm delete', 'tripetto'),
                __('Are you sure you want to delete the entry?', 'tripetto'),
                __('Yes, delete it', 'tripetto'),
                __('No, keep it', 'tripetto'),
                esc_attr($_REQUEST['page']),
                esc_attr($_REQUEST['id']),
                $item['id'],
                __('Delete', 'tripetto')
            )
        );

        return sprintf(
            '<a href="?page=%s&action=view&id=%s">%s %s</a>',
            esc_attr($_REQUEST['page']),
            $item['id'],
            esc_html('Entry ' . $item['id']),
            $this->row_actions($actions)
        );
    }

    /**
     * Render Creation date column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_creation($item)
    {
        return $item['created'];
    }

    /**
     * This method return columns to display in table
     *
     * @return array
     */
    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'id' => __('Entry ID', 'tripetto'),
            'creation' => __('Submitted', 'tripetto')
        );
        return $columns;
    }

    /**
     * This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'id' => array('id', true)
        );
        return $sortable_columns;
    }

    /**
     * Return array of bult actions if has any
     *
     * @return array
     */
    function get_bulk_actions()
    {
        $actions = array(
            'entries' => 'Delete'
        );
        return $actions;
    }

    /**
     * This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     *
     * @see $this->prepare_items()
     */
    function process_bulk_action()
    {
        if ('entries' === $this->current_action()) {
            $ids = isset($_REQUEST['entry_id'])
                ? $_REQUEST['entry_id']
                : array();

            if (is_array($ids)) {
                $ids = array_map('intval', $ids);
            } else {
                $ids = intval($ids);
            }

            Entry::delete($ids);
        }
    }

    /**
     * Prepare table list items.
     */
    public function prepare_items()
    {
        global $wpdb, $table_prefix;
        $table_name = $wpdb->prefix . 'tripetto_entries';
        $form_id = intval($_REQUEST['id']);
        $per_page = 25;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // will be used in pagination settings
        $total_items = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT COUNT(id) FROM $table_name WHERE form_id=%d",
                $form_id
            )
        );

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged'])
            ? max(0, intval($_REQUEST['paged'] - 1) * $per_page)
            : 0;
        $orderby =
            isset($_REQUEST['orderby']) &&
            in_array(
                $_REQUEST['orderby'],
                array_keys($this->get_sortable_columns())
            )
                ? $_REQUEST['orderby']
                : 'id';
        $order =
            isset($_REQUEST['order']) &&
            in_array($_REQUEST['order'], array('asc', 'desc'))
                ? $_REQUEST['order']
                : 'asc';

        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $this->items = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $table_name WHERE form_id = %d ORDER BY $orderby $order LIMIT %d OFFSET %d",
                $form_id,
                $per_page,
                $paged
            ),
            ARRAY_A
        );

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}
?>
