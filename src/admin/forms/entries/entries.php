<?php
include 'list.php';
include 'csv.php';

class AdminFormsEntries
{
    static function install()
    {
        AdminFormsEntries::upgrade(true);
    }

    static function upgrade($isInstall)
    {
        if ($isInstall || db_need_upgrade()) {
            global $wpdb;

            $table_name = $wpdb->prefix . 'tripetto_entries';
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                form_id mediumint(9) NOT NULL,
                entry text NOT NULL,
                fingerprint TINYTEXT NOT NULL,
                created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
            dbDelta($sql);
            db_verify();
        }
    }

    static function show($id)
    {
        $id = intval($id);

        global $wpdb; // this is how you get access to the database

        $table = $wpdb->prefix . "tripetto_forms";
        $name = esc_html(
            $wpdb->get_var(
                $wpdb->prepare("SELECT name from $table where id=%d", $id)
            )
        );

        if ($id > 0) {

            wp_enqueue_script('wp-tripetto-admin');

            $table = new AdminEntriesList();
            $table->prepare_items();

            $message = "";

            if (!empty($_REQUEST['entry_id'])) {
                $message =
                    '<div class="updated below-h2" id="message"><p>' .
                    sprintf(
                        __('Entries deleted: %d', 'tripetto'),
                        count(
                            isset($_REQUEST['entry_id'])
                                ? $_REQUEST['entry_id']
                                : []
                        )
                    ) .
                    '</p></div>';
            }
            ?>
        <div class="wrap">
            <h1 class="wp-heading-inline">
                <?php if ($name !== '') {
                    echo __('Entries: ' . esc_html($name), 'tripetto');
                } else {
                    echo __('Entries: Unnamed form', 'tripetto');
                } ?>
            </h1>
            <?php
            echo "<a href='?page=tripetto-forms' class='page-title-action'>Back to forms</a>";
            echo $message;

            $tablename = $wpdb->prefix . "tripetto_entries";
            $entry_groups = $wpdb->get_results(
                "SELECT fingerprint, count(*) as entryCount FROM $tablename WHERE form_id = $id GROUP BY fingerprint ORDER BY created ASC"
            );

            echo "<div>";
            if (count($entry_groups) == 1) {
                $entry_group = $entry_groups[0];
                echo "<a href='?form_id=$id&v=$entry_group->fingerprint' class='button action'>" .
                    __('Export entries as csv', 'tripetto') .
                    "</a>";
            } elseif (count($entry_groups) > 1) {
                echo "<select id='form_version'>";
                $version = count($entry_groups);
                foreach ($entry_groups as $entry_group) {
                    $entryCount = $entry_group->entryCount;
                    $entriesLabel = $entryCount == 1 ? 'entry' : 'entries';
                    echo "<option value='?form_id=$id&v=$entry_group->fingerprint'>" .
                        __('Version', 'tripetto') .
                        " $version ($entryCount $entriesLabel)</option>";
                    $version--;
                }
                echo "</select>";
                echo "<a onclick='location.href = document.getElementById(\"form_version\").value;' class='button action'>" .
                    __('Export entries as csv', 'tripetto') .
                    "</a>";
            }
            echo "</div>";
            ?>
            <form id="tripetto_entries_table" method="GET">
                <input type="hidden" name="page" value="<?php echo $_REQUEST[
                    'page'
                ]; ?>"/>
                <input type="hidden" name="id" value="<?php echo $_REQUEST[
                    'id'
                ]; ?>"/>
                <?php $table->display(); ?>
            </form>

        </div>
        <?php return;
        }

        echo __(
            "Something went wrong, could not fetch the results.",
            "tripetto"
        );
    }

    static function view($id)
    {
        $id = intval($id);

        if ($id > 0) {
            global $wpdb; // this is how you get access to the database

            $table = $wpdb->prefix . "tripetto_entries";
            $entry = $wpdb->get_row(
                $wpdb->prepare(
                    "SELECT form_id, entry, created from $table where id=%d",
                    $id
                )
            );

            $table_forms = $wpdb->prefix . "tripetto_forms";
            $name = esc_html(
                $wpdb->get_var(
                    $wpdb->prepare(
                        "SELECT name from $table_forms where id=%d",
                        $entry->form_id
                    )
                )
            );

            if (!is_null($entry->entry)) {
                wp_enqueue_script('wp-tripetto-admin');

                $data = json_decode($entry->entry);
                echo '<div class="wrap">';
                echo "<h1 class='wp-heading-inline'>Entry #$id: $name</h1>";
                echo "<a href='?page=tripetto-forms&action=entries&id=$entry->form_id' class='page-title-action'>Back to entries</a>";
                echo '

                <div id="poststuff">
                <div id="post-body" class="metabox-holder columns-2">
                <!-- main content -->
                <div id="post-body-content">
                <div class="meta-box-sortables ui-sortable">';

                foreach ($data->fields as $field) {
                    if ($field->string != "") {
                        echo '

                                <div class="postbox">
                                <h2><span>' .
                            $field->name .
                            '</span></h2>
                                <div class="inside">';

                        if (
                            ($field->type == "tripetto-block-file-upload" ||
                                $field->type == "file-upload") &&
                            !empty($field->reference)
                        ) {
                            echo "<a href='?attachment_id=$field->reference' target='_blank'>" .
                                $field->string .
                                "</a>";
                        } else {
                            echo '<p>' . $field->string . ' </p>';
                        }
                        echo '



                        </div>
                        </div>
                <!-- .postbox -->';
                    }
                }

                echo '

				</div>
				<!-- .meta-box-sortables .ui-sortable -->

			</div>
			<!-- post-body-content -->

			<!-- sidebar -->
			<div id="postbox-container-1" class="postbox-container">

				<div class="meta-box-sortables">

					<div class="postbox">

						<h2><span>' .
                    __('Single Entry Details', 'tripetto') .
                    '</span></h2>

                        <div class="inside">
                            <p><b>Entry:</b> #' .
                    $id .
                    '<br>
                            <b>Form:</b> ' .
                    $name .
                    '<br>
                            <b>Timestamp:</b> ' .
                    $entry->created .
                    '</b>
                    <br><br>
                            <a href="javascript:;" onclick="WPTripetto.showModal(\'' .
                    __('Confirm delete', 'tripetto') .
                    '\',\'' .
                    __(
                        'Are you sure you want to delete the entry?',
                        'tripetto'
                    ) .
                    '\',\'' .
                    __('Yes, delete it', 'tripetto') .
                    '\',\'' .
                    __('No, keep it', 'tripetto') .
                    '\',\'?page=tripetto-forms&action=entries&id=' .
                    $entry->form_id .
                    '&entry_id=' .
                    $id .
                    '\',400,210);" class="button-primary">' .
                    __('Delete Entry', 'tripetto') .
                    '</a>
						</div>
						<!-- .inside -->

					</div>
					<!-- .postbox -->

				</div>
				<!-- .meta-box-sortables -->

			</div>
			<!-- #postbox-container-1 .postbox-container -->

		</div>
		<!-- #post-body .metabox-holder .columns-2 -->

		<br class="clear">
	</div>
	<!-- #poststuff -->

                ';

                echo '</div>';

                return;
            }
        }

        echo __(
            "Something went wrong, could not fetch the result.",
            "tripetto"
        );
    }

    static function register($plugin)
    {
        register_activation_hook($plugin, 'AdminFormsEntries::install');
        add_action('plugins_loaded', 'AdminFormsEntries::upgrade');
    }
}
?>
