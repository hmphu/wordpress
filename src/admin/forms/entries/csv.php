<?php

function generate()
{
    if (!is_user_logged_in()) {
        return;
    }

    $id = isset($_REQUEST['form_id']) ? intval($_REQUEST['form_id']) : 0;
    $fingerprint = isset($_REQUEST['v']) ? $_REQUEST['v'] : '';

    if ($id > 0 && $fingerprint != '') {
        header("content-type: text/csv;charset=utf-8;");
        header(
            "content-disposition: attachment; filename=tripetto-export-$id-$fingerprint.csv"
        );

        $out = fopen('php://output', 'w');

        // select entries with fingerprint
        global $wpdb;
        global $wp;
        $homeUrl = home_url($wp->request);
        $table = $wpdb->prefix . "tripetto_entries";
        $entries = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT entry, created FROM $table WHERE form_id=%d AND fingerprint = %s ORDER BY created desc",
                $id,
                $fingerprint
            )
        );

        // add bom
        fputs($out, $bom = chr(0xef) . chr(0xbb) . chr(0xbf));

        $header = true;

        // loop entries
        foreach ($entries as $entry) {
            $fields = json_decode($entry->entry);

            if ($header) {
                $values = array_column($fields->fields, "name", "Created");
                array_unshift($values, "Created");
                fputcsv($out, $values);

                $header = false;
            }

            $values = array();
            array_push($values, $entry->created);
            foreach ($fields->fields as $field) {
                $value = $field->string;
                if (
                    ($field->type == "tripetto-block-file-upload" ||
                        $field->type == "file-upload") &&
                    isset($field->reference)
                ) {
                    $value = $homeUrl . '/?attachment_id=' . $field->reference;
                }
                array_push($values, $value);
            }
            fputcsv($out, $values);
        }

        fclose($out);
        exit();
    }
}

add_action('init', 'generate');
