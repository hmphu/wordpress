<?php
include 'attachments/attachments.php';
include 'list.php';
include 'edit/edit.php';
include 'entries/entries.php';

class AdminForms
{
    static function install()
    {
        AdminForms::upgrade(true);
    }

    static function upgrade($isInstall)
    {
        if ($isInstall || db_need_upgrade()) {
            global $wpdb;

            $table_name = $wpdb->prefix . 'tripetto_forms';
            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                name tinytext NOT NULL,
                definition longtext NOT NULL,
                collector tinytext NOT NULL DEFAULT '',
                collector_remove_branding tinyint NOT NULL DEFAULT -1,
                collector_style longtext NOT NULL DEFAULT '',
                confirmation_title text NOT NULL DEFAULT '',
                confirmation_subtitle text NOT NULL DEFAULT '',
                confirmation_text text NOT NULL DEFAULT '',
                confirmation_image text NOT NULL DEFAULT '',
                confirmation_button_label text NOT NULL DEFAULT '',
                confirmation_button_url text NOT NULL DEFAULT '',
                notification_email text NOT NULL DEFAULT '',
                notification_email_include_data tinyint NOT NULL DEFAULT 0,
                notification_slack text NOT NULL DEFAULT '',
                integration_zapier text NOT NULL DEFAULT '',
                premium tinyint NOT NULL DEFAULT 0,
                created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                modified timestamp,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
            dbDelta($sql);
            db_verify();
        }
    }

    static function menu()
    {
        add_submenu_page(
            'tripetto',
            __('All Forms', 'tripetto'),
            __('All Forms', 'tripetto'),
            'manage_options',
            'tripetto-forms',
            'AdminForms::page'
        );

        add_submenu_page(
            'tripetto',
            __('Add New', 'tripetto'),
            __('Add New', 'tripetto'),
            'manage_options',
            'tripetto-forms&action=create',
            'AdminForms::page'
        );
    }

    static function page()
    {
        wp_enqueue_style('wp-tripetto-admin');

        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";
        if ($action == "-1") {
            $action = isset($_REQUEST['action2']) ? $_REQUEST['action2'] : "";
        }

        $id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

        switch ($action) {
            case "create":
                AdminFormsEdit::create(
                    isset($_REQUEST['collector']) ? $_REQUEST['collector'] : ""
                );
                break;
            case "edit":
                AdminFormsEdit::edit($id);
                break;
            case "change":
                AdminFormsEdit::change(
                    $id,
                    isset($_REQUEST['collector']) ? $_REQUEST['collector'] : ""
                );
                break;
            case "entries":
                AdminFormsEntries::show($id);
                break;
            case "view":
                AdminFormsEntries::view($id);
                break;
            case "premium":
                License::markAsPremium($id);
            default:

                wp_enqueue_script('wp-tripetto-admin');

                $table = new AdminFormsList();
                $table->prepare_items();

                $message = "";

                if ($table->current_action() === "delete") {
                    $message =
                        '<div class="updated below-h2" id="message"><p>' .
                        sprintf(
                            __('Forms deleted: %d', 'tripetto'),
                            count(isset($_REQUEST['id']) ? $_REQUEST['id'] : [])
                        ) .
                        '</p></div>';
                }
                if ($table->current_action() === "duplicate") {
                    $message =
                        '<div class="updated below-h2" id="message"><p>' .
                        sprintf(
                            __('Forms duplicated: %d', 'tripetto'),
                            count(isset($_REQUEST['id']) ? $_REQUEST['id'] : [])
                        ) .
                        '</p></div>';
                }
                if ($table->current_action() === "premium") {
                    $message =
                        '<div class="updated below-h2" id="message"><p>' .
                        __(
                            'Form was successfully marked as Premium',
                            'tripetto'
                        ) .
                        '</p></div>';
                }
                ?>
                <div class="wrap">
                <?php if (!License::isInPremiumMode()) { ?>
                        <div style="background-color: #fcf8f3; border: 1px solid #e5e5e5; padding: 16px 10px; margin-bottom: 16px; margin-left: -10px; border-radius: 4px;">
                            <h3 style="margin: 0px; padding-bottom: 4px;">🎁 Get a FREE premium license for giving us your feedback</h3>
                            <p style="font-size: 14px; margin: 8px 0;">We are curious about what you think of Tripetto. We’ll give you a <strong>FREE one year premium license</strong> if you share your honest feedback with us.</p>
                            <a href="https://tripetto.app/collect/VRY9TNZBVD" target="_blank" style="font-size: 16px; text-decoration: none;">👉 Click here to share your feedback</a>
                            <p style="font-size: 14px; margin: 8px 0;">We will send you a premium license by email shortly after you submit your feedback. Many thanks in advance for helping us get better!</p>
                        </div>
                    <?php } ?>

                    <h1 class="wp-heading-inline">
                        <?php echo __('Tripetto Forms', 'tripetto'); ?>
                    </h1>
                    <a href="<?php echo "?page=" .
                        esc_attr($_REQUEST['page']) .
                        "&action=create"; ?>"
                        class="page-title-action">
                        <?php echo __('Create New Form', 'tripetto'); ?>
                    </a>
                    <hr class="wp-header-end">
                    <?php echo $message; ?>

                    <form id="tripetto_forms_table" method="GET">
                        <input type="hidden" name="page" value="<?php echo esc_attr(
                            $_REQUEST['page']
                        ); ?>"/>
                        <?php $table->display(); ?>

                    </form>
                </div>
                <?php break;
        }
    }

    static function register($plugin)
    {
        register_activation_hook($plugin, 'AdminForms::install');
        add_action('plugins_loaded', 'AdminForms::upgrade');

        AdminFormsEdit::register($plugin);
        AdminFormsEntries::register($plugin);
        AdminFormsAttachments::register($plugin);
    }
}
?>
