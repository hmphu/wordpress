<?php

// Extending default WP List Table Tripetto Class

class AdminFormsList extends WP_List_Table_Tripetto
{
    /**
     * Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     */
    public function __construct()
    {
        parent::__construct(array(
            'singular' => __('form', 'tripetto'),
            'plural' => __('forms', 'tripetto'),
            'ajax' => false
        ));
    }

    /**
     * This is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    /**
     * Render checkbox column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            esc_attr($item['id'])
        );
    }

    /**
     * Render name column with actions
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_name($item)
    {
        $actions = array(
            'edit' => sprintf(
                '<a href="?page=%s&action=edit&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Edit', 'tripetto')
            ),
            'duplicate' => sprintf(
                '<a href="?page=%s&action=duplicate&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Duplicate', 'tripetto')
            ),
            'entries' => sprintf(
                '<a href="?page=%s&action=entries&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('View Entries', 'tripetto')
            ),
            'delete' => sprintf(
                '<a href="javascript:;" onclick="WPTripetto.showModal(\'%s\',\'%s\',\'%s\',\'%s\',\'?page=%s&action=delete&id=%s\',400,210);">%s</a>',
                __('Confirm delete', 'tripetto'),
                __(
                    'All the data and the form itself will be removed. This action cannot be made undone.',
                    'tripetto'
                ),
                __('Yes, delete it', 'tripetto'),
                __('No, keep it', 'tripetto'),
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Delete', 'tripetto')
            )
        );

        $isInPremiumMode = License::isInPremiumMode();
        $hasPremiumFeatures = License::hasPremiumFeatures($item['id']);

        if (!$isInPremiumMode && !$hasPremiumFeatures) {
            array_push(
                $actions,
                sprintf(
                    '<a href="javascript:;" onclick="WPTripetto.showModal(\'%s\',\'%s\',\'%s\',\'%s\',\'?page=%s&action=premium&id=%s\',500,260,\'#2ecc71\');">%s</a>',
                    __('Mark as Premium', 'tripetto'),
                    __(
                        'You are about to designate this form as your complimentary Premium form. The premium settings (i.e. styles, notifications and integrations) for your current Premium form will be lost. Are you sure you want to switch?',
                        'tripetto'
                    ),
                    __('Yes, switch it', 'tripetto'),
                    __('No, stop', 'tripetto'),
                    esc_attr($_REQUEST['page']),
                    $item['id'],
                    __('Mark as Premium', 'tripetto')
                )
            );
        }

        return sprintf(
            '<a href="?page=%s&action=edit&id=%s">%s %s %s</a>',
            esc_attr($_REQUEST['page']),
            $item['id'],
            esc_html($item['name'] == "" ? "Unnamed form" : $item['name']),
            !$isInPremiumMode && $hasPremiumFeatures
                ? '<span class="premium">Premium</span>'
                : '',
            $this->row_actions($actions)
        );
    }

    /**
     * Render ID column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_id($item)
    {
        return $item['id'];
    }

    /**
     * Render Shortcode column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_shortcode($item)
    {
        return "<code>[tripetto id='" . $item['id'] . "']</code>";
    }

    /**
     * Render Creation date column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_creation($item)
    {
        return $item['created'];
    }

    /**
     * Render Modified date column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_modified($item)
    {
        return $item['modified'];
    }

    /**
     * Render Entries column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_entries($item)
    {
        $id = $item['id'];
        global $wpdb, $table_prefix;
        $table_name = $wpdb->prefix . 'tripetto_entries';
        $total_entries = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT COUNT(id) FROM $table_name WHERE form_id=%d",
                $id
            )
        );
        return "<a href='?page=tripetto-forms&action=entries&id={$id}'>$total_entries</a>";
    }

    /**
     * This method return columns to display in table
     *
     * @return array
     */
    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'name' => __('Name', 'tripetto'),
            'id' => __('ID', 'tripetto'),
            'shortcode' => __('Shortcode', 'tripetto'),
            'creation' => __('Created', 'tripetto'),
            'modified' => __('Modified', 'tripetto'),
            'entries' => __('Entries', 'tripetto')
        );
        return $columns;
    }

    /**
     * This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'name' => array('name', false),
            'id' => array('id', true)
        );
        return $sortable_columns;
    }

    /**
     * Return array of bult actions if has any
     *
     * @return array
     */
    function get_bulk_actions()
    {
        $actions = array(
            'delete' => 'Delete',
            'duplicate' => 'Duplicate'
        );
        return $actions;
    }

    /**
     * This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     *
     * @see $this->prepare_items()
     */
    function process_bulk_action()
    {
        global $wpdb;

        $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();

        if (is_array($ids)) {
            $ids = array_map('intval', $ids);
            $ids = implode(',', $ids);
        } else {
            $ids = intval($ids);
        }

        if ('delete' === $this->current_action()) {
            if (!empty($ids)) {
                $table_name = $wpdb->prefix . 'tripetto_entries';
                $entries = $wpdb->get_col(
                    "SELECT id FROM $table_name WHERE form_id IN ($ids)"
                );
                Entry::delete($entries);

                $table_name = $wpdb->prefix . 'tripetto_forms'; // do not forget about tables prefix
                $wpdb->query("DELETE FROM $table_name WHERE id IN ($ids)");
            }
        }
        if ('duplicate' === $this->current_action()) {
            if (!empty($ids)) {
                $table_name = $wpdb->prefix . 'tripetto_forms';
                $wpdb->query(
                    "INSERT INTO $table_name (name, definition) SELECT name, definition FROM $table_name WHERE id IN ($ids)"
                );
            }
        }
    }

    /**
     * Prepare table list items.
     */
    public function prepare_items()
    {
        global $wpdb, $table_prefix;
        $table_name = $wpdb->prefix . 'tripetto_forms';

        $per_page = 10;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // Validate premium
        License::validateMaximumPremiumForms();

        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged'])
            ? max(0, intval($_REQUEST['paged'] - 1) * $per_page)
            : 0;
        $orderby =
            isset($_REQUEST['orderby']) &&
            in_array(
                $_REQUEST['orderby'],
                array_keys($this->get_sortable_columns())
            )
                ? $_REQUEST['orderby']
                : 'id';
        $order =
            isset($_REQUEST['order']) &&
            in_array($_REQUEST['order'], array('asc', 'desc'))
                ? $_REQUEST['order']
                : 'asc';

        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $this->items = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d",
                $per_page,
                $paged
            ),
            ARRAY_A
        );

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}
?>
