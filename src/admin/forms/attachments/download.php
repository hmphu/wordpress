<?php

function download()
{
    if (!is_user_logged_in()) {
        return;
    }

    $id = isset($_REQUEST['attachment_id'])
        ? intval($_REQUEST['attachment_id'])
        : 0;

    if ($id > 0) {
        global $wpdb; // this is how you get access to the database
        $table = $wpdb->prefix . "tripetto_attachments";
        $attachment = $wpdb->get_row(
            $wpdb->prepare("SELECT * FROM $table WHERE id=%d", $id)
        );

        if (!is_null($attachment)) {
            header("content-type: $attachment->type");
            header("content-disposition: inline; filename=$attachment->name");

            $filename =
                $attachment->path .
                '/' .
                Attachment::formatFilename(
                    $attachment->name,
                    $attachment->form_id,
                    $id
                );
            readfile($filename);
            exit();
        }
    }
}

add_action('init', 'download');
