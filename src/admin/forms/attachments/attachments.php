<?php
include 'download.php';

class AdminFormsAttachments
{
    static function install()
    {
        AdminFormsAttachments::upgrade(true);

        // Update .htaccess
        if (!function_exists('get_home_path')) {
            include_once ABSPATH . '/wp-admin/includes/file.php';
        }

        if (!function_exists('insert_with_markers')) {
            include_once ABSPATH . '/wp-admin/includes/misc.php';
        }

        $htaccess = get_home_path() . '.htaccess';
        $lines = array(
            'RedirectMatch 404 ^/wp-content/uploads/.*/tripetto/.*$'
        );

        insert_with_markers($htaccess, 'Tripetto', $lines);
    }

    static function upgrade($isInstall)
    {
        if ($isInstall || db_need_upgrade()) {
            global $wpdb;

            $table_name = $wpdb->prefix . 'tripetto_attachments';
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                form_id mediumint(9) NOT NULL,
                entry_id mediumint(9) NULL,
                name tinytext NOT NULL,
                path tinytext NOT NULL,
                type tinytext NOT NULL,
                created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
            dbDelta($sql);
            db_verify();
        }
    }

    static function register($plugin)
    {
        if (is_admin()) {
            register_activation_hook($plugin, 'AdminFormsAttachments::install');
            add_action('plugins_loaded', 'AdminFormsAttachments::upgrade');
        }
    }
}
?>
