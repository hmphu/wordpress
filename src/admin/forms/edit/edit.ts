import { IDefinition, Debounce, assert, isBoolean, extend } from "tripetto-collector";
import { ISettings } from "../../../editor/settings";
import { header } from "./header";
import * as Superagent from "superagent";
import "./edit.scss";
import "bootstrap";

/**
 * Bootstraps a new edit view.
 * @param type Specifies the collector type.
 * @param version Contains the version.
 * @param id Specifies the identifier of the form.
 * @param definition Specifies the initial definition for the form.
 * @param baseUrl Specifies the base URL.
 * @param ajaxUrl Specifies the AJAX callback URL.
 * @param settings Specifies the settings.
 * @param isPremium Specifies if the premium tier is active.
 * @param showPremiumLabel Specifies if the premium label must be shown.
 */
export function bootstrap(
    type: "rolling" | "standard-bootstrap",
    version: string,
    id: number,
    definition: IDefinition | undefined,
    baseUrl: string,
    ajaxUrl: string,
    settings: ISettings = {
        collector: {},
        notifications: {},
        integrations: {}
    },
    isPremium: boolean = false,
    showPremiumLabel: boolean = false
): void {
    // Prepare the HTML elements
    const rootElement = document.createElement("div");
    const editorElement = document.createElement("div");
    const previewElement = document.createElement("div");
    let readyState = 0;
    const pkg = type === "standard-bootstrap" ? "TripettoCollectorStandardBootstrap" : "TripettoCollectorRolling";

    rootElement.id = "wp-tripetto-edit";

    if (window.innerWidth >= 600) {
        rootElement.classList.add("preview-mobile");
    }

    const headerActions = header(
        rootElement,
        id,
        type,
        definition && definition.name,
        () => {
            // Request the editor to open its settings panel
            editorFrame.postMessage("settings", window.location.origin);
        },
        (view: "preview" | "test") => {
            previewFrame.postMessage(
                {
                    name: "view",
                    value: view
                },
                window.location.origin
            );
        },
        () => {
            // Request the editor to open its tutorial panel
            editorFrame.postMessage("tutorial", window.location.origin);
        },
        showPremiumLabel
    );

    rootElement.appendChild(editorElement);
    rootElement.appendChild(previewElement);
    document.body.appendChild(rootElement);

    // Create the frames
    const editorFrame = assert(editorElement.appendChild(document.createElement("iframe")).contentWindow || undefined);
    const previewFrame = assert(previewElement.appendChild(document.createElement("iframe")).contentWindow || undefined);

    // Render the editor
    editorFrame.document.open();
    editorFrame.document.write(
        `<body><script src="${baseUrl}/vendors/tripetto-editor.js?ver=${version}"></script><script src="${baseUrl}/vendors/tripetto-editor-${type}.js?ver=${version}"></script><script src="${baseUrl}/scripts/wp-tripetto-editor.js?ver=${version}"></script><script>WPTripetto.editor(${pkg.replace(
            "TripettoCollector",
            "TripettoEditor"
        )},${JSON.stringify(
            extend<
                ISettings & {
                    type: "rolling" | "standard-bootstrap";
                    definition: IDefinition | undefined;
                    baseUrl: string;
                    isPremium: boolean;
                }
            >(
                {
                    type,
                    definition,
                    baseUrl,
                    isPremium
                },
                settings
            )
        )});</script></body>`
    );
    editorFrame.document.close();

    // Render the collector preview
    previewFrame.document.open();
    previewFrame.document.write(
        `<head><meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /><link rel="stylesheet" href="${baseUrl}/css/wp-tripetto-preview.css?ver=${version}" type="text/css" media="all"></head><body><script src="${baseUrl}/vendors/tripetto-collector.js?ver=${version}"></script><script src="${baseUrl}/vendors/tripetto-collector-${type}.js?ver=${version}"></script><script src="${baseUrl}/scripts/wp-tripetto-preview.js?ver=${version}"></script><script>WPTripetto.preview(${pkg},${JSON.stringify(
            {
                view: "preview",
                definition,
                style: (settings && isPremium && settings.collector.style) || undefined,
                overrides:
                    (settings &&
                        isPremium && {
                            removeBranding: settings.collector.removeBranding,
                            confirmationTitle: settings.collector.confirmationTitle,
                            confirmationSubtitle: settings.collector.confirmationSubtitle,
                            confirmationText: settings.collector.confirmationText,
                            confirmationImage: settings.collector.confirmationImage,
                            confirmationButton:
                                (settings.collector.confirmationTitle &&
                                    ((settings.collector.confirmationButtonLabel &&
                                        settings.collector.confirmationButtonUrl && {
                                            label: settings.collector.confirmationButtonLabel,
                                            url: settings.collector.confirmationButtonUrl
                                        }) ||
                                        "off")) ||
                                undefined
                        }) ||
                    undefined
            }
        )});</script></body>`
    );
    previewFrame.document.close();

    // Saves the definition on the server.
    const saveDefinition = new Debounce((d: IDefinition) => {
        Superagent.post(ajaxUrl)
            .type("form")
            .send({
                contentType: "json",
                action: "save_definition",
                id,
                name: d.name,
                definition: JSON.stringify(d)
            })
            .then(() => {
                headerActions.showSaveNotification();
            });
    }, 250);

    // Saves the settings on the server.
    const saveSettings = new Debounce((data: {}) => {
        Superagent.post(ajaxUrl)
            .type("form")
            .send(
                extend(data, {
                    contentType: "json",
                    action: "save_settings",
                    id
                })
            )
            .then(() => headerActions.showSaveNotification());
    }, 250);

    // Listen for messages from the editor or collector
    window.addEventListener("message", (e: MessageEvent) => {
        if (e.data) {
            if (e.data === "ready" || e.data.type === "ready") {
                readyState++;

                if (readyState === 2) {
                    rootElement.classList.add("ready");
                }

                return;
            }

            switch (e.data.type) {
                case "rename":
                    headerActions.updateName(e.data.name);
                    break;
                case "definition":
                    previewFrame.postMessage(
                        {
                            name: "definition",
                            value: e.data.definition
                        },
                        window.location.origin
                    );

                    saveDefinition.invoke(e.data.definition);
                    break;
                case "requestPreview":
                    previewFrame.postMessage(
                        {
                            name: "requestPreview",
                            id: e.data.id
                        },
                        window.location.origin
                    );
                    break;
                case "editRequest":
                    editorFrame.postMessage(
                        {
                            type: "editRequest",
                            id: e.data.id
                        },
                        window.location.origin
                    );
                    break;
                case "settings":
                    parseSettings(isPremium, e.data.settings, saveSettings, previewFrame);
                    break;
                case "settings:open":
                    headerActions.showSettings(true);
                    break;
                case "settings:close":
                    headerActions.showSettings(false);
                    break;
            }
        }
    });
}

/**
 * Parses the settings, sends them to the server and updates the preview frame.
 * @param isPremium Specifies if the premium tier is active.
 * @param settings Reference to the settings object.
 * @param fnSave Reference to the save debouncer.
 * @param preview Reference to the preview frame.
 */
function parseSettings(isPremium: boolean, settings: ISettings, fnSave: Debounce, preview: Window): void {
    const getBooleanValue = (value: boolean | undefined) => (isBoolean(value) ? (value ? 1 : 0) : -1);
    const changeSetting = (name: string, value: boolean | string | {} | undefined) => {
        preview.postMessage(
            {
                name,
                value
            },
            window.location.origin
        );
    };

    if (isPremium) {
        changeSetting("style", settings.collector.style);
        changeSetting("overrides", {
            removeBranding: settings.collector.removeBranding,
            confirmationTitle: settings.collector.confirmationTitle,
            confirmationSubtitle: settings.collector.confirmationSubtitle,
            confirmationText: settings.collector.confirmationText,
            confirmationImage: settings.collector.confirmationImage,
            confirmationButton:
                (settings.collector.confirmationTitle &&
                    ((settings.collector.confirmationButtonLabel &&
                        settings.collector.confirmationButtonUrl && {
                            label: settings.collector.confirmationButtonLabel,
                            url: settings.collector.confirmationButtonUrl
                        }) ||
                        "off")) ||
                undefined
        });
    }

    fnSave.invoke(
        extend<{}>(
            {
                "notification-email": settings.notifications.email || "",
                "notification-email-include-data": getBooleanValue(settings.notifications.includeFormDataInEmail)
            },
            isPremium
                ? {
                      "collector-remove-branding": getBooleanValue(settings.collector.removeBranding),
                      "collector-style": (settings.collector.style && JSON.stringify(settings.collector.style)) || "",
                      "collector-confirmation-title": settings.collector.confirmationTitle || "",
                      "collector-confirmation-subtitle": settings.collector.confirmationSubtitle || "",
                      "collector-confirmation-text": settings.collector.confirmationText || "",
                      "collector-confirmation-image": settings.collector.confirmationImage || "",
                      "collector-confirmation-button-label": settings.collector.confirmationButtonLabel || "",
                      "collector-confirmation-button-url": settings.collector.confirmationButtonUrl || "",
                      "notification-slack": settings.notifications.slack || "",
                      "integration-zapier": settings.integrations.zapier || ""
                  }
                : {}
        )
    );
}
