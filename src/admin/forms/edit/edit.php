<?php
class AdminFormsEdit
{
    static function scripts()
    {
        wp_register_script(
            'vendor-tripetto-collector',
            Helpers::plugin_url() . '/vendors/tripetto-collector.js',
            array(),
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );
    }

    static function create($collector)
    {
        global $wpdb; // this is how you get access to the database

        $table_forms = $wpdb->prefix . "tripetto_forms";

        switch ($collector) {
            case "standard-bootstrap":
                $collector = "standard-bootstrap";
                break;
            default:
                $collector = "rolling";
                break;
        }

        $wpdb->insert($table_forms, array(
            'id' => null,
            'collector' => $collector,
            'name' => __("Unnamed form"),
            'definition' => '{}',
            'premium' => 0
        ));

        $id = intval($wpdb->insert_id);

        if ($id > 0) {
            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE $table_forms SET modified=CURRENT_TIMESTAMP WHERE id=%d",
                    $id
                )
            );

            License::validateMaximumPremiumForms();

            $redirect = sprintf(
                'admin.php?page=%s&action=edit&id=%s',
                esc_attr($_REQUEST['page']),
                $id
            );

            echo '<br />' .
                __('🚀 Creating new form...', 'tripetto') .
                '<script type="text/javascript">window.location="' .
                $redirect .
                '";</script>';

            return;
        }

        echo __(
            "Something went wrong, could not create a new form (" .
                $wpdb->last_error .
                ").",
            "tripetto"
        );
    }

    static function edit($id)
    {
        $id = intval($id);

        if ($id > 0) {
            global $wpdb; // this is how you get access to the database

            $table = $wpdb->prefix . "tripetto_forms";
            $row = $wpdb->get_row(
                $wpdb->prepare("SELECT * from $table where id=%d", $id)
            );

            if (!is_null($row)) {
                $isInPremiumMode = License::isInPremiumMode();
                $hasPremiumFeatures = License::hasPremiumFeatures($id);

                $settings = new stdClass();
                $settings->collector = new stdClass();
                $settings->notifications = new stdClass();
                $settings->integrations = new stdClass();

                if ($row->notification_email != "") {
                    $settings->notifications->email = $row->notification_email;
                }

                if ($row->notification_email_include_data >= 0) {
                    $settings->notifications->includeFormDataInEmail =
                        $row->notification_email_include_data > 0
                            ? true
                            : false;
                }

                if ($hasPremiumFeatures) {
                    if ($row->collector_remove_branding >= 0) {
                        $settings->collector->removeBranding =
                            $row->collector_remove_branding > 0 ? true : false;
                    }

                    if (
                        $row->collector_style != "" &&
                        $row->collector_style != "{}"
                    ) {
                        $settings->collector->style = json_decode(
                            $row->collector_style
                        );
                    }

                    if ($row->confirmation_title != "") {
                        $settings->collector->confirmationTitle =
                            $row->confirmation_title;
                    }

                    if ($row->confirmation_subtitle != "") {
                        $settings->collector->confirmationSubtitle =
                            $row->confirmation_subtitle;
                    }

                    if ($row->confirmation_text != "") {
                        $settings->collector->confirmationText =
                            $row->confirmation_text;
                    }

                    if ($row->confirmation_image != "") {
                        $settings->collector->confirmationImage =
                            $row->confirmation_image;
                    }

                    if ($row->confirmation_button_label != "") {
                        $settings->collector->confirmationButtonLabel =
                            $row->confirmation_button_label;
                    }

                    if ($row->confirmation_button_url != "") {
                        $settings->collector->confirmationButtonUrl =
                            $row->confirmation_button_url;
                    }

                    if ($row->notification_slack != "") {
                        $settings->notifications->slack =
                            $row->notification_slack;
                    }

                    if ($row->integration_zapier != "") {
                        $settings->integrations->zapier =
                            $row->integration_zapier;
                    }
                }

                echo '<br />' . __('⌛ Loading...', 'tripetto');

                AdminFormsEdit::render(
                    $row->collector,
                    $id,
                    $row->definition,
                    $settings,
                    $hasPremiumFeatures,
                    $hasPremiumFeatures && !$isInPremiumMode
                );

                return;
            }
        }

        echo __("Something went wrong, could not fetch the form.", "tripetto");
    }

    static function change($id, $collector)
    {
        $id = intval($id);

        switch ($collector) {
            case "standard-bootstrap":
                $collector = "standard-bootstrap";
                break;
            default:
                $collector = "rolling";
                break;
        }

        if ($id > 0) {
            global $wpdb;

            $table = $wpdb->prefix . "tripetto_forms";

            $query = "UPDATE $table SET collector='$collector' WHERE id = $id";
            $wpdb->query($query);

            $redirect = sprintf(
                'admin.php?page=%s&action=edit&id=%s',
                esc_attr($_REQUEST['page']),
                $id
            );

            echo '<br />' .
                __('⌛ Changing the collector...', 'tripetto') .
                '<script type="text/javascript">window.location="' .
                $redirect .
                '";</script>';
        }

        echo __("Something went wrong, could not change the form.", "tripetto");
    }

    static function saveDefinition()
    {
        $definition = isset($_POST['definition'])
            ? wp_strip_all_tags(wp_unslash($_POST['definition']))
            : "";
        $name = isset($_POST['name'])
            ? sanitize_text_field($_POST['name'])
            : "";
        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;

        if ($id > 0 && $definition != "") {
            global $wpdb; // this is how you get access to the database

            $table_name = $wpdb->prefix . "tripetto_forms";
            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE $table_name SET name=%s,definition=%s,modified=CURRENT_TIMESTAMP WHERE id=%d",
                    $name,
                    $definition,
                    $id
                )
            );
        }

        die(); // this is required to return a proper result
    }

    static function saveSettings()
    {
        global $wpdb; // this is how you get access to the database

        $table_name = $wpdb->prefix . "tripetto_forms";
        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;

        if ($id > 0) {
            $notificationsEmail = isset($_POST['notification-email'])
                ? sanitize_email(wp_unslash($_POST['notification-email']))
                : "";
            $notificationEmailIncludeData = isset(
                $_POST['notification-email-include-data']
            )
                ? intval($_POST['notification-email-include-data'])
                : -1;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE $table_name SET notification_email=%s,notification_email_include_data=%d,modified=CURRENT_TIMESTAMP WHERE id=%d",
                    $notificationsEmail,
                    $notificationEmailIncludeData,
                    $id
                )
            );

            $hasPremiumFeatures = License::hasPremiumFeatures($id);

            if ($hasPremiumFeatures) {
                $collectorRemoveBranding = isset(
                    $_POST['collector-remove-branding']
                )
                    ? intval($_POST['collector-remove-branding'])
                    : -1;
                $collectorStyle = isset($_POST['collector-style'])
                    ? wp_strip_all_tags(wp_unslash($_POST['collector-style']))
                    : "";

                $confirmationTitle = isset(
                    $_POST['collector-confirmation-title']
                )
                    ? sanitize_text_field(
                        wp_unslash($_POST['collector-confirmation-title'])
                    )
                    : "";
                $confirmationSubtitle = isset(
                    $_POST['collector-confirmation-subtitle']
                )
                    ? sanitize_text_field(
                        wp_unslash($_POST['collector-confirmation-subtitle'])
                    )
                    : "";
                $confirmationText = isset($_POST['collector-confirmation-text'])
                    ? sanitize_textarea_field(
                        wp_unslash($_POST['collector-confirmation-text'])
                    )
                    : "";
                $confirmationImage = isset(
                    $_POST['collector-confirmation-image']
                )
                    ? sanitize_text_field(
                        wp_unslash($_POST['collector-confirmation-image'])
                    )
                    : "";
                $confirmationButtonLabel = isset(
                    $_POST['collector-confirmation-button-label']
                )
                    ? sanitize_text_field(
                        wp_unslash(
                            $_POST['collector-confirmation-button-label']
                        )
                    )
                    : "";
                $confirmationButtonUrl = isset(
                    $_POST['collector-confirmation-button-url']
                )
                    ? sanitize_text_field(
                        wp_unslash($_POST['collector-confirmation-button-url'])
                    )
                    : "";

                $notificationsSlack = isset($_POST['notification-slack'])
                    ? sanitize_text_field(
                        wp_unslash($_POST['notification-slack'])
                    )
                    : "";
                $intergrationsZapier = isset($_POST['integration-zapier'])
                    ? sanitize_text_field(
                        wp_unslash($_POST['integration-zapier'])
                    )
                    : "";

                $wpdb->query(
                    $wpdb->prepare(
                        "UPDATE $table_name SET collector_remove_branding=%d,collector_style=%s,confirmation_title=%s,confirmation_subtitle=%s,confirmation_text=%s,confirmation_image=%s,confirmation_button_label=%s,confirmation_button_url=%s,notification_slack=%s,integration_zapier=%s WHERE id=%d",
                        $collectorRemoveBranding,
                        $collectorStyle,
                        $confirmationTitle,
                        $confirmationSubtitle,
                        $confirmationText,
                        $confirmationImage,
                        $confirmationButtonLabel,
                        $confirmationButtonUrl,
                        $notificationsSlack,
                        $intergrationsZapier,
                        $id
                    )
                );
            }
        }

        die(); // this is required to return a proper result
    }

    private static function render(
        $collector,
        $id,
        $definition,
        $settings,
        $hasPremiumFeatures,
        $showPremiumLabel
    ) {
        $hasPremiumFeatures = $hasPremiumFeatures ? 1 : 0;
        $showPremiumLabel = $showPremiumLabel ? 1 : 0;

        $collector =
            isset($collector) && $collector != "" ? $collector : "rolling";
        $version = $GLOBALS["TRIPETTO_PLUGIN_VERSION"];
        $base = Helpers::plugin_url();
        $definition = isset($definition) ? $definition : "undefined";
        $settings = isset($settings) ? json_encode($settings) : "undefined";

        wp_enqueue_script('vendor-tripetto-collector');
        wp_enqueue_script('wp-tripetto-admin');

        wp_add_inline_script(
            'wp-tripetto-admin',
            "WPTripetto.bootstrap(\"$collector\", \"$version\", $id, $definition, \"$base\", ajaxurl, $settings, $hasPremiumFeatures, $showPremiumLabel);"
        );
    }

    static function register($plugin)
    {
        add_action('wp_ajax_save_definition', 'AdminFormsEdit::saveDefinition');
        add_action('wp_ajax_save_settings', 'AdminFormsEdit::saveSettings');
        add_action('admin_enqueue_scripts', 'AdminFormsEdit::scripts');
    }
}
?>
