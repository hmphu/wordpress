import { _, Debounce } from "tripetto-collector";

function composeButton(button: HTMLButtonElement, icon?: string, label?: string): [HTMLElement, HTMLSpanElement] {
    const buttonIcon = document.createElement("i");
    const buttonLabel = document.createElement("span");

    if (icon && icon !== "") {
        buttonIcon.setAttribute("class", "fas fa-" + icon + " fa-fw");
        button.appendChild(buttonIcon);
    }

    if (label && label !== "") {
        buttonLabel.textContent = _(label);
        buttonLabel.setAttribute("class", "d-none d-lg-inline-block" + (icon && icon !== "" ? " ml-1" : ""));
        button.appendChild(buttonLabel);
    }

    return [buttonIcon, buttonLabel];
}

function addSaved(parent: HTMLDivElement): HTMLDivElement {
    const element = document.createElement("div");
    const icon = document.createElement("i");
    const label = document.createElement("span");

    element.setAttribute("class", "saved mr-3");
    icon.setAttribute("class", "fas fa-save fa-fw mr-1");
    label.textContent = _("Saved");

    element.appendChild(icon);
    element.appendChild(label);

    return parent.appendChild(element);
}

function addSettingsButton(parent: HTMLDivElement, openSettings: () => void): HTMLButtonElement {
    const groupElement = document.createElement("div");
    const button = document.createElement("button");

    groupElement.setAttribute("class", "btn-group btn-group-sm btn-group-toggle mr-2");

    button.setAttribute("class", "btn btn-outline-t_header");
    button.onclick = () => openSettings();

    composeButton(button, "palette", _("Style & settings"));
    groupElement.appendChild(button);
    parent.appendChild(groupElement);

    return button;
}

function addViewButton(
    parent: HTMLDivElement,
    initial: "preview" | "test",
    changeView: (view: "preview" | "test") => void
): HTMLButtonElement {
    const fnLabel = (view: "preview" | "test") => {
        switch (view) {
            case "preview":
                return _("Editing");
            case "test":
                return _("Testing");
        }
    };
    const fnIcon = (view: "preview" | "test") => {
        switch (view) {
            case "preview":
                return "edit";
            case "test":
                return "running";
        }
    };
    const groupElement = document.createElement("div");
    const button = document.createElement("button");
    const buttonContent = composeButton(button, fnIcon(initial), fnLabel(initial));
    const menu = document.createElement("div");
    const menuHeader = document.createElement("h6");
    const menuPreview = document.createElement("a");
    const menuTest = document.createElement("a");
    const fnChange = (view: "preview" | "test") => {
        buttonContent[0].setAttribute("class", `fas fa-${fnIcon(view)} fa-fw`);
        buttonContent[1].textContent = fnLabel(view);

        changeView(view);
    };

    groupElement.setAttribute("class", "btn-group btn-group-sm mr-2 dropdown");
    button.setAttribute("class", "btn btn-outline-t_header dropdown-toggle");
    button.setAttribute("type", "button");
    button.setAttribute("id", "viewDropdown");
    button.setAttribute("data-toggle", "dropdown");
    button.setAttribute("aria-haspopup", "true");
    button.setAttribute("aria-expanded", "false");

    menu.setAttribute("class", "dropdown-menu");
    menu.setAttribute("aria-labelledby", "viewDropdown");

    menuHeader.setAttribute("class", "dropdown-header");
    menuPreview.setAttribute("class", "dropdown-item");
    menuPreview.onclick = () => fnChange("preview");
    menuTest.setAttribute("class", "dropdown-item");
    menuTest.onclick = () => fnChange("test");

    menuHeader.textContent = _("Preview mode");
    menuPreview.textContent = fnLabel("preview");
    menuTest.textContent = fnLabel("test");

    menu.appendChild(menuHeader);
    menu.appendChild(menuPreview);
    menu.appendChild(menuTest);
    groupElement.appendChild(button);
    groupElement.appendChild(menu);
    parent.appendChild(groupElement);

    return button;
}

function addCollectorButton(
    parent: HTMLDivElement,
    initial: "rolling" | "standard-bootstrap",
    rootElemement: HTMLDivElement,
    id: number
): HTMLButtonElement {
    const fnLabel = (collector: "rolling" | "standard-bootstrap") => {
        switch (collector) {
            case "rolling":
                return _("Rolling UI");
            case "standard-bootstrap":
                return _("Standard UI");
        }
    };
    const groupElement = document.createElement("div");
    const button = document.createElement("button");
    const buttonContent = composeButton(button, "caret-square-right", fnLabel(initial));
    const menu = document.createElement("div");
    const menuHeader = document.createElement("h6");
    const menuRolling = document.createElement("a");
    const menuStandardBootstrap = document.createElement("a");
    const fnChange = (collector: "rolling" | "standard-bootstrap") => {
        buttonContent[0].setAttribute("class", `fas fa-caret-square-right fa-fw`);
        buttonContent[1].textContent = fnLabel(collector);

        rootElemement.remove();

        window.open(window.location.pathname + `?page=tripetto-forms&action=change&id=${id}&collector=${collector}`, "_self");
    };

    groupElement.setAttribute("class", "btn-group btn-group-sm mr-2 dropdown");
    button.setAttribute("class", "btn btn-outline-t_header dropdown-toggle");
    button.setAttribute("type", "button");
    button.setAttribute("id", "viewDropdown");
    button.setAttribute("data-toggle", "dropdown");
    button.setAttribute("aria-haspopup", "true");
    button.setAttribute("aria-expanded", "false");

    menu.setAttribute("class", "dropdown-menu");
    menu.setAttribute("aria-labelledby", "viewDropdown");

    menuHeader.setAttribute("class", "dropdown-header");
    menuRolling.setAttribute("class", "dropdown-item");
    menuRolling.onclick = () => fnChange("rolling");
    menuStandardBootstrap.setAttribute("class", "dropdown-item");
    menuStandardBootstrap.onclick = () => fnChange("standard-bootstrap");

    menuHeader.textContent = _("Collector UI");
    menuRolling.textContent = fnLabel("rolling");
    menuStandardBootstrap.textContent = fnLabel("standard-bootstrap");

    menu.appendChild(menuHeader);
    menu.appendChild(menuRolling);
    menu.appendChild(menuStandardBootstrap);
    groupElement.appendChild(button);
    groupElement.appendChild(menu);
    parent.appendChild(groupElement);

    return button;
}

function addPreviewButton(
    parent: HTMLDivElement,
    initial: "off" | "mobile" | "tablet" | "desktop",
    changePreview: (preview: "off" | "mobile" | "tablet" | "desktop") => void
): (preview: "off" | "mobile" | "tablet" | "desktop") => void {
    const fnLabel = (preview: "off" | "mobile" | "tablet" | "desktop") => {
        switch (preview) {
            case "off":
                return _("Hide preview");
            case "mobile":
                return _("Mobile");
            case "tablet":
                return _("Tablet");
            case "desktop":
                return _("Desktop");
        }
    };
    const fnIcon = (preview: "off" | "mobile" | "tablet" | "desktop") => {
        switch (preview) {
            case "off":
                return "eye-slash";
            case "mobile":
                return "mobile-alt";
            case "tablet":
                return "tablet-alt";
            case "desktop":
                return "desktop";
        }
    };
    const groupElement = document.createElement("div");
    const button = document.createElement("button");
    const buttonContent = composeButton(button, fnIcon(initial), fnLabel(initial));
    const menu = document.createElement("div");
    const menuHeader = document.createElement("h6");
    const menuDivider = document.createElement("div");
    const menuOff = document.createElement("a");
    const menuMobile = document.createElement("a");
    const menuTablet = document.createElement("a");
    const menuDesktop = document.createElement("a");
    const fnChange = (preview: "off" | "mobile" | "tablet" | "desktop") => {
        buttonContent[0].setAttribute("class", `fas fa-${fnIcon(preview)} fa-fw`);
        buttonContent[1].textContent = fnLabel(preview);

        changePreview(preview);
    };

    groupElement.setAttribute("class", "btn-group btn-group-sm mr-2 dropdown");
    button.setAttribute("class", "btn btn-outline-t_header dropdown-toggle");
    button.setAttribute("type", "button");
    button.setAttribute("id", "previewDropdown");
    button.setAttribute("data-toggle", "dropdown");
    button.setAttribute("aria-haspopup", "true");
    button.setAttribute("aria-expanded", "false");

    menu.setAttribute("class", "dropdown-menu");
    menu.setAttribute("aria-labelledby", "previewDropdown");

    menuHeader.setAttribute("class", "dropdown-header");
    menuDivider.setAttribute("class", "dropdown-divider");
    menuOff.setAttribute("class", "dropdown-item");
    menuOff.onclick = () => fnChange("off");
    menuMobile.setAttribute("class", "dropdown-item");
    menuMobile.onclick = () => fnChange("mobile");
    menuTablet.setAttribute("class", "dropdown-item");
    menuTablet.onclick = () => fnChange("tablet");
    menuDesktop.setAttribute("class", "dropdown-item");
    menuDesktop.onclick = () => fnChange("desktop");

    menuHeader.textContent = _("Preview for");
    menuOff.textContent = fnLabel("off");
    menuMobile.textContent = fnLabel("mobile");
    menuTablet.textContent = fnLabel("tablet");
    menuDesktop.textContent = fnLabel("desktop");

    menu.appendChild(menuHeader);
    menu.appendChild(menuMobile);
    menu.appendChild(menuTablet);
    menu.appendChild(menuDesktop);
    menu.appendChild(menuDivider);
    menu.appendChild(menuOff);
    groupElement.appendChild(button);
    groupElement.appendChild(menu);
    parent.appendChild(groupElement);

    return fnChange;
}

function addBackButton(rootElemement: HTMLDivElement): HTMLButtonElement {
    const button = document.createElement("button");

    button.setAttribute("class", "btn btn-t_header btn-sm mr-2");
    button.onclick = () => {
        rootElemement.remove();

        window.open(window.location.pathname + "?page=tripetto-forms", "_self");
    };

    composeButton(button, "arrow-circle-left");

    return button;
}

function addFullscreenToggle(parent: HTMLDivElement, rootElement: HTMLDivElement): void {
    const button = document.createElement("button");

    button.setAttribute("class", "btn btn-outline-t_header");
    button.setAttribute("data-toggle", "button");
    button.onclick = () => {
        rootElement.classList.toggle("fullscreen");
        button.classList.toggle("active", rootElement.classList.contains("fullscreen"));
    };

    composeButton(button, "arrows-alt");

    parent.appendChild(button);
}

function addTutorialButton(parent: HTMLDivElement, openTutorial: () => void): void {
    const button = document.createElement("button");

    button.setAttribute("class", "btn btn-outline-t_header");
    button.onclick = () => openTutorial();

    composeButton(button, "question");

    parent.appendChild(button);
}

function addCloseButton(parent: HTMLDivElement, rootElemement: HTMLDivElement): void {
    const button = document.createElement("button");

    button.setAttribute("class", "btn btn-danger");
    button.onclick = () => {
        rootElemement.remove();

        window.open(window.location.pathname + "?page=tripetto-forms", "_self");
    };

    composeButton(button, "times");

    parent.appendChild(button);
}

function getName(name: string | undefined): string {
    return name || _("Unnamed form");
}

export function header(
    rootElement: HTMLDivElement,
    id: number,
    collector: "rolling" | "standard-bootstrap",
    name: string | undefined,
    openSettings: () => void,
    changeView: (view: "preview" | "test") => void,
    openTutorial: () => void,
    showPremiumLabel: boolean
): {
    showSaveNotification: () => void;
    showSettings: (active: boolean) => void;
    updateName: (name: string) => void;
} {
    // Header elements
    const header = document.createElement("div");
    const headerLeft = document.createElement("div");
    const headerRight = document.createElement("div");

    headerLeft.setAttribute("class", "header-left px-2");
    headerRight.setAttribute("class", "d-flex justify-content-end px-2");

    // Title
    const title = document.createElement("h1");
    title.setAttribute("class", "px-2");
    title.textContent = getName(name);
    title.onclick = () => openSettings();

    // Premium
    const premium = document.createElement("span");
    premium.setAttribute("class", "premium");
    premium.textContent = "premium";

    // Saved
    const saved = addSaved(headerRight);

    // Buttons
    const settingsButton = addSettingsButton(headerRight, openSettings);
    const changePreview = addPreviewButton(
        headerRight,
        window.innerWidth >= 600 ? "mobile" : "off",
        (preview: "off" | "mobile" | "tablet" | "desktop") => {
            rootElement.classList.toggle("preview-mobile", preview === "mobile");
            rootElement.classList.toggle("preview-tablet", preview === "tablet");
            rootElement.classList.toggle("preview-desktop", preview === "desktop");
        }
    );

    addViewButton(headerRight, "preview", changeView);
    addCollectorButton(headerRight, collector, rootElement, id);

    // Controls
    const controlsButtonGroup = document.createElement("div");
    controlsButtonGroup.setAttribute("class", "btn-group btn-group-sm btn-group-toggle");

    addFullscreenToggle(controlsButtonGroup, rootElement);
    addTutorialButton(controlsButtonGroup, () => openTutorial());
    addCloseButton(controlsButtonGroup, rootElement);

    // Append elements to side (left/right)
    headerLeft.appendChild(addBackButton(rootElement));
    headerLeft.appendChild(title);
    if (showPremiumLabel) {
        headerLeft.appendChild(premium);
    }
    headerRight.appendChild(controlsButtonGroup);

    // Append sides to header
    header.appendChild(headerLeft);
    header.appendChild(headerRight);

    // Append header to root
    rootElement.appendChild(header);

    const fnResize = () => {
        if (
            window.innerWidth < 600 &&
            (rootElement.classList.contains("preview-tablet") || rootElement.classList.contains("preview-desktop"))
        ) {
            changePreview("off");
        }

        if (
            window.innerWidth < 992 &&
            (rootElement.classList.contains("preview-tablet") || rootElement.classList.contains("preview-desktop"))
        ) {
            changePreview("mobile");
        }

        if (window.innerWidth < 1200 && rootElement.classList.contains("preview-desktop")) {
            changePreview("tablet");
        }
    };

    window.addEventListener("load", () => fnResize());
    window.addEventListener("resize", () => fnResize());
    window.addEventListener("orientationchange", () => fnResize());

    const depulse = new Debounce(() => saved.classList.remove("pulse"), 2000);

    return {
        showSaveNotification: () => {
            saved.classList.remove("pulse");
            requestAnimationFrame(() => saved.classList.add("pulse"));
            depulse.invoke();
        },
        showSettings: (active: boolean) => settingsButton.classList.toggle("active", active),
        updateName: (name: string) => (title.textContent = getName(name))
    };
}
