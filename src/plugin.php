<?php
/*
Plugin Name: {{ NAME }}
Description: {{ DESCRIPTION }}
Plugin URI: {{ HOMEPAGE }}
Author: Tripetto
Text Domain: tripetto
Domain path: /languages
Version: {{ VERSION }}
*/
if (!defined('ABSPATH')) {
    exit();
}

if (function_exists('t_freemius')) {
    t_freemius()->set_basename(true, __FILE__);
} else {
    if (!function_exists('t_freemius')) {
        // Create a helper function for easy SDK access.
        function t_freemius()
        {
            global $t_freemius;

            if (!isset($t_freemius)) {
                // Include Freemius SDK.
                require_once dirname(__FILE__) . '/freemius/start.php';

                $t_freemius = fs_dynamic_init(array(
                    'id' => '3825',
                    'slug' => 'tripetto',
                    'type' => 'plugin',
                    'public_key' => 'pk_bbe3e39e20ddf86c6ff4721c5e30e',
                    'secret_key' => '',
                    'is_premium' => true,
                    'premium_suffix' => 'Premium',
                    // If your plugin is a serviceware, set this option to false.
                    'has_premium_version' => true,
                    'has_addons' => false,
                    'has_paid_plans' => true,
                    'menu' => array(
                        'slug' => 'tripetto',
                        'contact' => false,
                        'support' => false
                    )
                ));
            }

            return $t_freemius;
        }

        // Init Freemius.
        t_freemius();
        // Signal that SDK was initiated.
        do_action('t_freemius_loaded');
    }

    $GLOBALS["TRIPETTO_PLUGIN_VERSION"] = "{{ VERSION }}";

    // Libraries
    include 'lib/database.php';
    include 'lib/helpers.php';
    include 'lib/wp-list-table.php';
    include 'lib/attachment.php';
    include 'lib/entry.php';
    include 'lib/license.php';
    include 'lib/uninstall.php';
    include 'lib/template.php';

    // Components
    include 'admin/admin.php';
    include 'collector/collector.php';

    // Register our components
    Admin::register(__FILE__);
    Collector::register(__FILE__);

    // Hooks for database creation and maintenance
    add_action('plugins_loaded', 'License::upgrade');
    add_action('plugins_loaded', 'db_upgrade');
    add_action('activate_blog', 'db_install');
    register_activation_hook(__FILE__, 'db_install');

    // Uninstall hook
    t_freemius()->add_action('after_uninstall', 'uninstall');
}
?>
