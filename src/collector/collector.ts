import { Export, IDefinition, Instance, assert, extend, castToBoolean } from "tripetto-collector";
import { handleAttachment } from "./attachment";
import * as Superagent from "superagent";

interface IProps {
    /** Specifies the definition of the form. */
    readonly definition: IDefinition;

    /** Specifies the style of the form. */
    readonly style?: {
        showNavigation?: boolean;
        showScrollbar?: boolean;
    };

    /** Contains the collector package to use. */
    readonly wp_collector: "standard-bootstrap" | "rolling";

    /** Contains the plugin version. */
    readonly wp_version: string;

    /** Specifies the mode of implementation. */
    readonly wp_mode: "inline" | "iframe" | "overlay";

    /** Specifies the width. */
    readonly wp_width: string;

    /** Specifies the height. */
    readonly wp_height: string;

    /** Specifies the id of the form. */
    readonly wp_id: number;

    /** Specifies the AJAX callback URL. */
    readonly wp_url: string;

    /** Contains the plugin base URL. */
    readonly wp_base: string;

    /** Specifies the nonce. */
    readonly wp_nonce: string;
}

interface IPackage {
    readonly run: ({}) => {
        readonly height: number;
    };
}

/** Available collectors. */
declare const TripettoCollectorRolling: IPackage;
declare const TripettoCollectorStandardBootstrap: IPackage;

/** Specifies if the event listener is active. */
let hasEventListener = false;

/**
 * Initializes a collector
 * @param props Specifies the properties for the collector.
 */
export function init(props: IProps): void {
    const element = document.getElementById(`tripetto-collector-${props.wp_nonce}`);

    if (element) {
        const pkg = props.wp_collector === "standard-bootstrap" ? TripettoCollectorStandardBootstrap : TripettoCollectorRolling;
        const pkgName = props.wp_collector === "standard-bootstrap" ? "TripettoCollectorStandardBootstrap" : "TripettoCollectorRolling";

        if (props.wp_mode === "overlay") {
            element.style.position = "fixed";
            element.style.left = "0";
            element.style.top = "0";
            element.style.right = "0";
            element.style.bottom = "0";
            element.style.zIndex = "99999999";
            element.style.maxWidth = "none";
            element.style.margin = "0";
        } else {
            element.style.position = "relative";
            element.style.width = props.wp_width;

            if (props.wp_mode !== "inline" || props.wp_collector === "rolling") {
                element.style.height = props.wp_height === "auto" ? "1px" : props.wp_height;
                element.style.overflow = "hidden";
            }
        }

        if (props.wp_mode === "inline") {
            if (props.wp_collector === "rolling" && props.style && !castToBoolean(props.style.showNavigation, true)) {
                element.style.left = "-5px";
            }

            run(pkg, props, element, props.wp_collector === "rolling" && props.wp_height === "auto");
        } else {
            const frame = document.createElement("iframe");

            if (
                props.wp_mode !== "overlay" &&
                (props.wp_collector !== "rolling" || (props.style && !castToBoolean(props.style.showNavigation, true)))
            ) {
                element.style.left = "-5px";
            }

            frame.setAttribute("allowTransparency", "true");
            frame.style.width = "100%";
            frame.style.height = "100%";
            frame.style.border = "none";

            const frameRef = assert(element.appendChild(frame).contentWindow || undefined);

            frameRef.document.open();
            frameRef.document.write(
                `<head>` +
                    `<meta charset="UTF-8" />` +
                    `<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />` +
                    `<style>` +
                    `body { margin: 0; overflow: hidden; ${
                        props.wp_collector !== "rolling" && (props.wp_height !== "auto" || props.wp_mode === "overlay")
                            ? "overflow-y: auto; "
                            : ""
                    }}${props.wp_mode === "overlay" ? ".tripetto-collector-standard-bootstrap { min-height: 100%; }" : ""}` +
                    `</style>` +
                    `</head>` +
                    `<body>` +
                    `<script src="${props.wp_base}/vendors/tripetto-collector.js?ver=${props.wp_version}"></script>` +
                    `<script src="${props.wp_base}/vendors/tripetto-collector-${props.wp_collector}.js?ver=${props.wp_version}"></script>` +
                    `<script src="${props.wp_base}/scripts/wp-tripetto-collector.js?ver=${props.wp_version}"></script>` +
                    `<script>WPTripetto.run(${pkgName},${JSON.stringify(props)},document.body,${
                        props.wp_height === "auto" && props.wp_mode !== "overlay" ? "true" : "false"
                    },true);</script>` +
                    `</body>`
            );
            frameRef.document.close();

            if (!hasEventListener) {
                hasEventListener = true;

                window.addEventListener(
                    "message",
                    (e: MessageEvent) => {
                        if (e.data && e.data.nonce && e.data.height) {
                            const el = document.getElementById(`tripetto-collector-${e.data.nonce}`);

                            if (el) {
                                el.style.height = e.data.height;
                            }
                        }
                    },
                    false
                );
            }
        }
    }
}

/**
 * Runs the collector.
 * @param pkg Reference to the collector package.
 * @param props Specifies the properties for the collector.
 */
export async function run(
    pkg: IPackage,
    props: IProps,
    element: HTMLElement,
    autoHeight: boolean,
    isFrame: boolean = false
): Promise<void> {
    let height = 0;
    const collector = await pkg.run(
        extend<{}>(props, {
            element,
            usage: props.wp_mode === "overlay" ? "standalone" : isFrame ? "frame" : "inline",
            autoResize: props.wp_mode !== "inline",
            autoFocus: props.wp_mode === "overlay",
            onChange:
                autoHeight &&
                (() => {
                    if (collector && collector.height !== height) {
                        height = collector.height;

                        if (props.wp_mode === "inline") {
                            element.style.height = `${collector.height}px`;
                        } else {
                            window.parent.postMessage(
                                {
                                    nonce: props.wp_nonce,
                                    height: `${collector.height + 5}px`
                                },
                                window.location.origin
                            );
                        }
                    }
                }),
            onAttachment: handleAttachment(props.wp_id, props.wp_url),
            onFinish: (instance: Instance) => {
                const fields = Export.fields(instance);

                Superagent.post(props.wp_url)
                    .type("form")
                    .send({
                        action: "save_entry",
                        fields: JSON.stringify(fields),
                        id: props.wp_id
                    })
                    .then(() => {});
            }
        })
    );
}
