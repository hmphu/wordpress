import * as Superagent from "superagent";

const nonces: { [key: string]: string } = {};

export const handleAttachment = (id: number, url: string) => ({
    put: async (file: File, onProgress: (percent?: number) => void) => {
        const formData = new FormData();

        formData.append("action", "upload_attachment");
        formData.append("file", file);
        formData.append("id", `${id}`);

        return Superagent.post(url)
            .send(formData)
            .on("progress", (event: Superagent.ProgressEvent) => {
                if (event.direction === "upload") {
                    onProgress(event.percent);
                }
            })
            .then((res: Superagent.Response) => {
                const data = JSON.parse(res.text);
                if (data.response === "success" && data.id && data.nonce) {
                    nonces[data.id] = data.nonce;
                    return Promise.resolve(data.id);
                } else {
                    return Promise.reject(data.error);
                }
            });
    },
    get: () => {
        return Promise.reject("Not implemented because restoring attachments via snapshots is not supported.");
    },
    delete: async (attachmentId: string) => {
        const nonce = nonces[attachmentId];

        return Superagent.post(url)
            .type("form")
            .send({
                action: "delete_attachment",
                id: attachmentId,
                nonce
            })
            .then((res: Superagent.Response) => {
                const data = JSON.parse(res.text);
                if (data.response === "success") {
                    return Promise.resolve();
                } else {
                    return Promise.reject(data.error);
                }
            });
    }
});
