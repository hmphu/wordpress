<?php
class Collector
{
    static function scripts()
    {
        $plugin_url = Helpers::plugin_url();

        wp_register_script(
            'vendor-tripetto-collector',
            $plugin_url . '/vendors/tripetto-collector.js',
            array(),
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );
        wp_register_script(
            'vendor-tripetto-collector-rolling',
            $plugin_url . '/vendors/tripetto-collector-rolling.js',
            array(),
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );
        wp_register_script(
            'vendor-tripetto-collector-standard-bootstrap',
            $plugin_url . '/vendors/tripetto-collector-standard-bootstrap.js',
            array(),
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );
        wp_register_script(
            'wp-tripetto-collector',
            $plugin_url . '/scripts/wp-tripetto-collector.js',
            array(),
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );
    }

    static function shortCode($atts)
    {
        extract(
            $atts = shortcode_atts(
                array(
                    'id' => '0',
                    'mode' => 'iframe',
                    'width' => '100%',
                    'height' => 'auto'
                ),
                $atts,
                'tripetto'
            )
        );

        $id = intval($atts['id']);

        if ($id > 0) {
            global $wpdb;

            $table_name = $wpdb->prefix . "tripetto_forms";
            $row = $wpdb->get_row(
                $wpdb->prepare("SELECT * FROM $table_name WHERE id=%d", $id)
            );

            if (!is_null($row)) {
                static $count = 0;
                static $eventListener = 0;

                $count++;
                $props = new stdClass();

                $props->definition = $row->definition;
                $props->wp_version = $GLOBALS["TRIPETTO_PLUGIN_VERSION"];
                $props->wp_mode = $atts['mode'];
                $props->wp_height = $atts['width'];
                $props->wp_height = $atts['height'];
                $props->wp_id = intval($row->id);
                $props->wp_nonce = $nonce = hash(
                    "crc32",
                    "tripetto-collector-" . $count
                );
                $props->wp_url =
                    admin_url('admin-ajax.php') .
                    "?nonce=" .
                    wp_create_nonce(Collector::createSaveEntryNonceAction($id));
                $props->wp_base = Helpers::plugin_url();

                switch ($row->collector) {
                    case "standard-bootstrap":
                        $props->wp_collector = "standard-bootstrap";
                        break;
                    default:
                        $props->wp_collector = "rolling";
                        break;
                }

                if (License::hasPremiumFeatures($id)) {
                    if (
                        $row->collector_style != "" &&
                        $row->collector_style != "{}"
                    ) {
                        $props->style = json_decode($row->collector_style);
                    }

                    if (
                        $row->collector_remove_branding >= 0 ||
                        $row->confirmation_title != ""
                    ) {
                        $props->overrides = new stdClass();

                        if ($row->collector_remove_branding >= 0) {
                            $props->overrides->removeBranding =
                                $row->collector_remove_branding > 0
                                    ? true
                                    : false;
                        }

                        if ($row->confirmation_title != "") {
                            $props->overrides->confirmationTitle =
                                $row->confirmation_title;

                            if ($row->confirmation_subtitle != "") {
                                $props->overrides->confirmationSubtitle =
                                    $row->confirmation_subtitle;
                            }

                            if ($row->confirmation_text != "") {
                                $props->overrides->confirmationText =
                                    $row->confirmation_text;
                            }

                            if ($row->confirmation_image != "") {
                                $props->overrides->confirmationImage =
                                    $row->confirmation_image;
                            }

                            $props->overrides->confirmationButton = "off";

                            if (
                                $row->confirmation_button_label != "" &&
                                $row->confirmation_button_url != ""
                            ) {
                                $props->overrides->confirmationButton = new stdClass();

                                $props->overrides->confirmationButton->label =
                                    $row->confirmation_button_label;
                                $props->overrides->confirmationButton->url =
                                    $row->confirmation_button_url;
                            }
                        }
                    }
                }

                wp_enqueue_script('vendor-tripetto-collector');
                wp_enqueue_script(
                    'vendor-tripetto-collector-' . $props->wp_collector
                );
                wp_enqueue_script('wp-tripetto-collector');

                $props = json_encode($props);

                wp_add_inline_script(
                    'wp-tripetto-collector',
                    "(function(t){t(t)})(function(t){return typeof WPTripetto===\"undefined\"?setTimeout(function(){t(t)},1):WPTripetto.init($props)});"
                );

                return '<div id="tripetto-collector-' . $nonce . '"></div>';
            }
        }

        return "Invalid shortcode!";
    }

    static function saveEntry()
    {
        $fields = wp_strip_all_tags(
            wp_unslash(isset($_POST['fields']) ? $_POST['fields'] : "")
        );
        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;

        if (
            isset($_REQUEST['nonce']) &&
            wp_verify_nonce(
                $_REQUEST['nonce'],
                Collector::createSaveEntryNonceAction($id)
            ) &&
            $id > 0 &&
            $fields != ""
        ) {
            global $wpdb; // this is how you get access to the database

            $table_name = $wpdb->prefix . "tripetto_forms";
            $form = $wpdb->get_row("SELECT * FROM $table_name WHERE id=$id");

            if (!is_null($form)) {
                $fields_obj = json_decode($fields);
                $wpdb->insert($wpdb->prefix . "tripetto_entries", array(
                    'id' => null,
                    'entry' => $fields,
                    'fingerprint' => $fields_obj->fingerprint,
                    'form_id' => $form->id
                ));
                $entry_id = $wpdb->insert_id;
                $pluginUrl = Helpers::plugin_url();
                $formName = $form->name == "" ? "Unnamed form" : $form->name;

                // Check for attachments and confirm them in the database
                foreach ($fields_obj->fields as $field) {
                    $reference = isset($field->reference)
                        ? $field->reference
                        : '';

                    if (
                        ($field->type == "tripetto-block-file-upload" ||
                            $field->type == "file-upload") &&
                        !empty($reference)
                    ) {
                        $wpdb->update(
                            $wpdb->prefix . "tripetto_attachments",
                            array('entry_id' => $entry_id),
                            array('id' => $reference)
                        );
                    }

                    if (
                        $field->type == 'tripetto-block-mailer' &&
                        $field->slot == 'recipient'
                    ) {
                        $node = $field->node;
                        $recipient = $field->string;

                        $nodeFields = array_filter(
                            $fields_obj->fields,
                            function ($value, $key) use ($node) {
                                return $value->node == $node;
                            },
                            ARRAY_FILTER_USE_BOTH
                        );

                        $subjectFields = array_filter(
                            $nodeFields,
                            function ($value, $key) {
                                return $value->slot == 'subject';
                            },
                            ARRAY_FILTER_USE_BOTH
                        );
                        $subject = reset($subjectFields)->string;

                        $messageFields = array_filter(
                            $nodeFields,
                            function ($value, $key) {
                                return $value->slot == 'message';
                            },
                            ARRAY_FILTER_USE_BOTH
                        );
                        $message = reset($messageFields)->string;

                        $footer = '';
                        if ($form->collector_remove_branding != 1) {
                            $footer = Template::renderEmailFooterTemplate(
                                $recipient,
                                $pluginUrl
                            );
                        }

                        $replacements = array(
                            'footer' => $footer,
                            'message' => nl2br($message)
                        );
                        $template = Template::render(
                            '/views/emails/mailer.php',
                            $replacements
                        );

                        Collector::sendMail($recipient, $subject, $template);
                    }
                }

                // Our entry is stored, now execute actions and hooks

                // Send notification email
                if ($form->notification_email != "") {
                    $adminurl = admin_url('admin.php');
                    $recipient = $form->notification_email;
                    $subject = sprintf("New submission from %s", $formName);

                    $footer = Template::renderEmailFooterTemplate(
                        $recipient,
                        $pluginUrl
                    );
                    $message = '';
                    $replacements = array(
                        'footer' => $footer,
                        'name' => $formName,
                        'entryUrl' => sprintf(
                            '%s?page=tripetto-forms&action=view&id=%d',
                            $adminurl,
                            $entry_id
                        )
                    );

                    $formattedFields = '';
                    if ($form->notification_email_include_data == 1) {
                        $data = json_decode($fields);

                        foreach ($data->fields as $field) {
                            if (
                                $field->string != "" &&
                                $field->type != 'tripetto-block-mailer'
                            ) {
                                $formattedFields .= sprintf(
                                    '<p><b>%s</b><br>',
                                    $field->name
                                );

                                if (
                                    $field->type == 'file-upload' &&
                                    !empty($field->reference)
                                ) {
                                    $formattedFields .= sprintf(
                                        '<a href="%s?attachment_id=%s" target="_blank">%s</a>',
                                        $adminurl,
                                        $field->reference,
                                        $field->string
                                    );
                                } else {
                                    $formattedFields .= $field->string;
                                }

                                $formattedFields .= '</p>';
                            }
                        }

                        if ($formattedFields == '') {
                            $formattedFields =
                                '<p><i>The submission is empty.</i></p>';
                        }
                    }

                    $replacements['data'] = $formattedFields;
                    $message = Template::render(
                        '/views/emails/notification.php',
                        $replacements
                    );

                    Collector::sendMail($recipient, $subject, $message);
                }

                if (License::hasPremiumFeatures($id)) {
                    if ($form->notification_slack != "") {
                        // Slack notification
                        $message = array(
                            'payload' => json_encode(array(
                                'text' =>
                                    __(
                                        'You have received a new submission from form ',
                                        'tripetto'
                                    ) . $formName
                            ))
                        );
                        // Post the message to Slack
                        wp_remote_post($form->notification_slack, array(
                            'method' => 'POST',
                            'timeout' => 30,
                            'redirection' => 5,
                            'httpversion' => '1.0',
                            'blocking' => true,
                            'headers' => array(),
                            'body' => $message,
                            'cookies' => array()
                        ));
                    }

                    if ($form->integration_zapier != "") {
                        // Zapier integration
                        $message = json_encode($fields);
                        // Post the result to Zapier
                        wp_remote_post($form->integration_zapier, array(
                            'method' => 'POST',
                            'timeout' => 30,
                            'redirection' => 5,
                            'httpversion' => '1.0',
                            'blocking' => true,
                            'headers' => array(
                                'Content-Type' =>
                                    'application/json; charset=utf-8'
                            ),
                            'body' => $message,
                            'cookies' => array()
                        ));
                    }
                }
            }
        }

        die(); // this is required to return a proper result
    }

    static function sendMail($recipient, $subject, $message)
    {
        $headers = array(
            'Content-Type: text/html; charset=UTF-8',
            'From:' .
                get_bloginfo('name') .
                ' <' .
                get_bloginfo('admin_email') .
                '>'
        );

        $result = wp_mail($recipient, $subject, $message, $headers);
    }

    static function uploadAttachment()
    {
        $response = array();
        $formId = isset($_POST['id']) ? intval($_POST['id']) : 0;
        $file = $_FILES['file'];

        if ($formId > 0 && isset($file)) {
            global $wpdb;

            // Check for existing form.
            $table_name = $wpdb->prefix . "tripetto_forms";
            $form = $wpdb->get_row(
                "SELECT * FROM $table_name WHERE id=$formId"
            );

            if (!is_null($form)) {
                $filename = sanitize_file_name($file['name']);

                add_filter('upload_dir', 'Collector::changeUploadDir');
                $uploadDir = wp_upload_dir();

                // Create attachment record into the database.
                $wpdb->insert($wpdb->prefix . "tripetto_attachments", array(
                    'id' => null,
                    'form_id' => $formId,
                    'name' => $filename,
                    'path' => $uploadDir['path'],
                    'type' => sanitize_mime_type($file['type'])
                ));
                $attachmentId = $wpdb->insert_id;

                // Change the filename.
                $file['name'] = Attachment::formatFilename(
                    $filename,
                    $formId,
                    $attachmentId
                );

                // Save the file to disk.
                $overrides = array(
                    'test_form' => false,
                    'test_type' => false,
                    'action' => 'upload_attachment'
                );
                $uploaded_file = wp_handle_upload($file, $overrides);

                if ($uploaded_file && !isset($uploaded_file['error'])) {
                    $response['response'] = 'success';
                    $response['id'] = $attachmentId;
                    $response['nonce'] = wp_create_nonce(
                        Collector::createDeleteAttachmentNonceAction(
                            $attachmentId
                        )
                    );
                } else {
                    $response['response'] = 'error';
                    $response['error'] = $uploaded_file['error'];
                }
            } else {
                $response['error'] = 'Form is required!';
            }
        } else {
            $response['error'] = 'Input is invalid.';
        }

        echo json_encode($response);
        die();
    }

    static function deleteAttachment()
    {
        $response = array();

        $attachmentId = isset($_POST['id']) ? intval($_POST['id']) : 0;
        $nonce = $_POST['nonce'];

        if (
            $attachmentId > 0 &&
            wp_verify_nonce(
                $nonce,
                Collector::createDeleteAttachmentNonceAction($attachmentId)
            )
        ) {
            Attachment::delete($attachmentId, false);
            $response['response'] = 'success';
        } else {
            $response['error'] = 'attachment id is invalid';
        }

        echo json_encode($response);
        die();
    }

    static function createDeleteAttachmentNonceAction($attachmentId)
    {
        return 'delete_attachment_' . $attachmentId;
    }

    static function createSaveEntryNonceAction($formId)
    {
        return 'save_entry_' . $formId;
    }

    static function changeUploadDir($param)
    {
        $customDir = '/tripetto';
        $param['subdir'] = $param['subdir'] . $customDir;
        $param['path'] = $param['path'] . $customDir;
        return $param;
    }

    static function register($plugin)
    {
        add_action('wp_enqueue_scripts', 'Collector::scripts');
        add_shortcode('tripetto', 'Collector::shortCode');

        add_action('wp_ajax_save_entry', 'Collector::saveEntry');
        add_action('wp_ajax_nopriv_save_entry', 'Collector::saveEntry');

        add_action('wp_ajax_upload_attachment', 'Collector::uploadAttachment');
        add_action(
            'wp_ajax_nopriv_upload_attachment',
            'Collector::uploadAttachment'
        );

        add_action('wp_ajax_delete_attachment', 'Collector::deleteAttachment');
        add_action(
            'wp_ajax_nopriv_delete_attachment',
            'Collector::deleteAttachment'
        );
    }
}
?>
