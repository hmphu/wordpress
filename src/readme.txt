=== {{ TITLE }} ===
Contributors: tripetto
Tags: {{ KEYWORDS }}
Requires at least: 4.9.10
Tested up to: 5.2.3
Requires PHP: 5.6.20
Stable tag: {{ VERSION }}
License: GPLv2 or later

{{ DESCRIPTION }}

== Description ==

**_Just another form builder? No, not at all!_**

Use Tripetto to do it all just a bit better, without a single line of code. All inside your own WP Admin. No third-party account needed, not even a Tripetto account. GDPR proof.

### 🧐 What makes Tripetto different? ###

💡 Let’s start with the **visual form builder** to easily create your forms and surveys. Ideal for small forms, but even when you need **advanced logic**, your form structure stays understandable and maintainable. And with the **realtime preview** you always see what you’re working on.

💡 While building your form, choose how you want to present it in your WP site: in a more **traditional** way like a contact form, or a **conversational** way where each question gets the attention it deserves, one by one. It’s up to you. And with the **theming** options you can style your form/survey to match the design of your site.

💡 Implement your form in your WP site with the **shortcode** we provide. *That's it!*

### 💁 Let us help you get going ###
- Take a look in our <a href="https://wordpress.tripetto.com/sandbox/" rel="friend" title="Free Tripetto Sandbox" target="_blank">free Sandbox</a> for a live demo and example forms;
- Visit our <a href="https://tripetto.com/wordpress/" rel="friend" title="More about Tripetto for WordPress" target="_blank">WordPress page</a> for more information;
- Visit our <a href="https://tripetto.com/wordpress/get-started/" rel="friend" title="Get Started with Tripetto for WordPress" target="_blank">Get Started page</a> for instructions, templates and tutorials.

### 👷 Easily build your forms ###
- Visual form building on a drawing board, unlike any other form builder;
- All question types you need. See FAQ section below for the full list;
- Advanced logic, but easy to use;
- Realtime preview while building you form.

### 🖥️ Show your forms how you want ###
- Deploy forms and surveys to your WP site with a shortcode;
- Choose between a standard and conversational way to collect;
- Style your forms;
- Fullscreen overlay option.

### 🛡️ Handle responses safely ###
- Responses are stored in your own WordPress install only (no third-party);
- View and manage results inside your WP Admin;
- Export to CSV;
- Automated email and Slack notifications after form completion;
- Zapier integration for connecting to 1.500+ apps.

### 👑 Premium features ###
Upgrade the Tripetto WordPress plugin to greatly enhance all your forms and surveys.
Premium license includes:
- Theming and styling;
- Custom goodbye page;
- Remove Tripetto branding;
- Slack notifications;
- Zapier integration;
- Updates and support.

👉 **<a href="https://tripetto.com/wordpress/get-started/" rel="friend" title="Get your premium license" target="_blank">Get your Premium license today!</a>**

**Always FREE for one form**

All standard and premium features will be completely FREE for one form or survey of your choosing. Upgrade to premium to get the premium features for your other creations.

### 🎁 Free premium license for giving feedback ###

We’ll give you a **FREE one year premium license** if you share your honest feedback with us. Just install, activate and use the plugin. Then click the link on the Tripetto dashboard in your WP Admin to open the feedback form.

We will send you a premium license by email shortly after you submit your feedback. Many thanks in advance for helping us get better!

### 🔔 Stay up-to-date ###
- <a href="https://tripetto.com/subscribe/" rel="friend" title="Subscribe to Tripetto newsletter" target="_blank">Subscribe to our newsletter</a>
- <a href="https://www.twitter.com/tripetto/" rel="friend" title="Follow Tripetto on Twitter" target="_blank">Follow us on Twitter</a>
- <a href="https://www.facebook.com/tripetto/" rel="friend" title="Like Tripetto on Facebook" target="_blank">Like us on Facebook</a>

== Installation ==

👉 Install Tripetto via the WordPress.org plugin repository or by uploading the files to your server;

👉 Activate the plugin through the Plugins page in your WP Admin;

👉 Navigate to the Tripetto tab at the bottom of your WP Admin menu.

You can make as many forms as you’d like in the free version. The free version also contains all the premium features for one form of your choosing.

👑 Get your <a href="https://tripetto.com/wordpress/get-started/" rel="friend" target="_blank">Premium License</a> to upgrade all your forms!

== Frequently Asked Questions ==

= Can I use Tripetto for free? =

Yes, you can! You can make as many forms as you’d like in the free version. The free version also contains all the premium features for one form of your choosing.

= Which question types are included? =

**Input blocks**
Tripetto naturally supports all the commonly used form question types (i.e. building blocks).
- Static Text (paragraph, quote);
- Standard text fields (single and multiple lines);
- Dedicated text fields (email, number, password, URL);
- Dropdown;
- Radiobuttons;
- Multiple checkboxes;
- Single checkbox;
- Multiple choice;
- Rating;
- Yes/No;
- File Upload;
- Matrix.

**Action blocks**
Enhance your forms with powerful action blocks to perform smart tasks in the background.
- Mailer - <a href="https://medium.com/tripetto/your-form-is-now-a-mail-agent-74b5c01edd5b" rel="friend" title="Blog post about mailer field" target="_blank">More info</a>;
- Hidden field - <a href="https://medium.com/tripetto/hidden-fields-in-tripetto-264a666b1ec7" rel="friend" title="Blog post about hidden field" target="_blank">More info</a>.

**Condition blocks**
Make forms smart by intelligently and automatically directing flows with dynamic condition blocks.
- Device Size.

= How do I embed forms in my WP site? =

Embedding works with a shortcode which you can add at any desired place on a page in your WP project. Read our <a href="https://tripetto.com/wordpress/get-started/" rel="friend" title="See instructions" target="_blank">instructions</a> for more information about the shortcode.

= How do I upgrade to premium? =

There are a few scenarios to upgrade. Generally, you need to take the following steps:
- You need a **zip package** with the premium version and your **personal license key**. After purchasing, you can find these in Freemius (our upgrade partner) and/or in your email;
- Purchase the upgrade via our <a href="https://tripetto.com/wordpress/get-started/" rel="friend" title="Upgrade to premium" target="_blank">website</a>, or your WP Admin menu;
- Upload, install and activate the zip package in your WP Admin. Enter the license key to activate.

= Can I request new features, like specific additional question types? =

Yes, of course. We’d love to hear what you think we should add to make Tripetto better. Please <a href="https://tripetto.com/contact/" rel="friend" title="Contact us" target="_blank">drop us a line</a>.

= Can I contribute to the plugin? =

Yes, you can. This plugin is open source. You can find the code on <a href="https://gitlab.com/tripetto/wordpress/" rel="friend" title="Open source on Gitlab" target="_blank">Gitlab</a>. We are always open for merge requests.

= Can I implement the form builder in my own software? =

Indeed you can! We provide a full SDK to implement the form builder into your own software. Check the <a href="https://tripetto.com/sdk/" rel="friend" title="More info about the Tripetto SDK" target="_blank">SDK page</a> on our website for all information and documentation (pricing may apply to implement the editor).

== Screenshots ==

1. Demo of a form in a conversational presentation, showing questions one by one
2. Demo of a form in a conversational presentation, showing questions one by one
3. Demo of a form in a standard presentation, ideal for small forms (like contact forms)
4. Demo of the visual form builder, with easy to use logic
5. Screenshot of the visual form builder in WP Admin, with the realtime form preview
6. Example of a Support form, embedded in a WP site
7. Example of a BBQ Invitation form, embedded in a WP site
8. Example of a Product Evaluation form, embedded in a WP site

== Changelog ==

{{ CHANGELOG }}

== Upgrade Notice ==

* None
