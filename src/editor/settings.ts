import {
    Components,
    Debounce,
    Editor,
    Forms,
    Layers,
    IEditorStyle,
    castToString,
    castToBoolean,
    isBoolean,
    _,
    isString,
    isFilledString,
    isObject,
    each,
    filter,
    findFirst,
    getAny,
    destroy,
    setAny,
    map,
    SHA2_256
} from "tripetto";

export interface ISettings {
    readonly collector: {
        removeBranding?: boolean;
        style?: {};
        confirmationTitle?: string;
        confirmationSubtitle?: string;
        confirmationText?: string;
        confirmationImage?: string;
        confirmationButtonLabel?: string;
        confirmationButtonUrl?: string;
    };

    readonly notifications: {
        email?: string;
        includeFormDataInEmail?: boolean;
        slack?: string;
    };

    readonly integrations: {
        zapier?: string;
    };
}

export interface IStyle {
    type: string;
    dependsOn?: string;
    name: string;
    title: string;
    description?: string;
    options?: Forms.IDropdownOption<string>[];
    buttons?: Forms.IRadiobutton<string>[];
    default?: string | boolean;
}

export interface IStyles {
    title: string;
    description?: string;
    optional?: boolean;
    base?: string;
    dependsOn?: string;
    styles: IStyle[];
}

export type TStyles = IStyles[];

export class SettingsComponent extends Components.Controller<{
    readonly editor: Editor;
    readonly settings: ISettings;
    readonly styles?: TStyles;
    readonly isPremium: boolean;
}> {
    private change = new Debounce(() => {
        if (this.whenChanged) {
            this.whenChanged(this.ref.settings);
        }
    });

    whenClosed?: () => void;
    whenChanged?: (settings: ISettings) => void;

    private static addStyle(
        style: IStyle,
        value: string | boolean | undefined,
        fnChange: (newValue: string | boolean) => void
    ): Forms.Control[] {
        switch (style.type) {
            case "label":
                return [
                    new Forms.Text("singleline", castToString(value))
                        .label(style.title)
                        .placeholder(_("Specify a label"))
                        .on((label: Forms.Text) => fnChange(label.value))
                ];
            case "checkbox":
                return [
                    new Forms.Checkbox(style.title, castToBoolean(value)).on((checkbox: Forms.Checkbox) => fnChange(checkbox.isChecked))
                ];
            case "dropdown":
            case "dropdown-custom":
                const customIdentifier = style.type === "dropdown-custom" ? SHA2_256("dropdown-custom") : "";
                const isCustomValue =
                    (customIdentifier &&
                        !findFirst(
                            style.options,
                            (option: Forms.IDropdownOption<string>) => "value" in option && option.value === castToString(value)
                        )) ||
                    false;
                const dropdownField = new Forms.Dropdown(
                    [
                        ...(style.options || []),
                        ...((customIdentifier && [
                            {
                                label: _("Custom"),
                                value: customIdentifier
                            }
                        ]) ||
                            [])
                    ],
                    isCustomValue ? customIdentifier : castToString(value)
                )
                    .label(style.title)
                    .on((dropdown: Forms.Dropdown<string>) => {
                        if (customField) {
                            customField.visible(dropdown.value === customIdentifier);
                        }

                        if (!customField || dropdown.value !== customIdentifier) {
                            fnChange(dropdown.value || "");
                        }
                    });
                const customField =
                    customIdentifier &&
                    new Forms.Text("singleline", isCustomValue ? castToString(value) : "")
                        .placeholder(_("Custom"))
                        .visible(isCustomValue)
                        .on((label: Forms.Text) => {
                            if (dropdownField.value === customIdentifier) {
                                fnChange(label.value);
                            }
                        });

                return customField ? [dropdownField, customField] : [dropdownField];
            case "radiobuttons":
                return [
                    new Forms.Radiobutton(style.buttons || [], castToString(value))
                        .label(style.title)
                        .on((radiobutton: Forms.Radiobutton<string>) => fnChange(radiobutton.value || ""))
                ];
            case "color":
                return [
                    new Forms.Text("singleline", castToString(value))
                        .label(style.title)
                        .placeholder(_("Specify hex color or color name"))
                        .on((color: Forms.Text) => fnChange(color.value))
                ];
            case "image":
                return [
                    new Forms.Text("singleline", castToString(value))
                        .sanitize(false)
                        .trim(false)
                        .label(style.title)
                        .placeholder(_("Specify image URL"))
                        .on((image: Forms.Text) => fnChange(image.value))
                ];
            default:
                return [new Forms.Static(style.title)];
        }
    }

    private static getValue(value: string | boolean | undefined, def: string | boolean | undefined): string | boolean | undefined {
        return value || isBoolean(value) ? value : def;
    }

    private static addPremiumNotification(feature: boolean = true): Forms.Notification {
        return new Forms.Notification(
            feature
                ? _("You need a paid subscription to use this feature or you could mark this form as your single free Premium form.")
                : _(
                      "You need a paid subscription before you can use some of the features below or you could mark this form as your single free Premium form."
                  ),
            "warning"
        );
    }
    static open(
        layer: Layers.Layer,
        editor: Editor,
        style: IEditorStyle,
        styles: TStyles | undefined,
        settings: ISettings | undefined,
        isPremium: boolean
    ): SettingsComponent {
        return new SettingsComponent(layer, editor, style, styles, settings, isPremium);
    }

    private constructor(
        layer: Layers.Layer,
        editor: Editor,
        style: IEditorStyle,
        styles: TStyles | undefined,
        settings: ISettings | undefined,
        isPremium: boolean
    ) {
        super(
            layer,
            {
                editor,
                styles,
                settings: settings || {
                    collector: {},
                    notifications: {},
                    integrations: {}
                },
                isPremium
            },
            _("Style and settings"),
            "normal",
            style
        );

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private get settings(): ISettings {
        return this.ref.settings;
    }

    private changed(): void {
        if (this.layer.isReady) {
            this.change.invoke();
        }
    }

    private addNameFeature(cards: Components.Cards.Cards): void {
        cards.features
            .option(
                _("Name"),
                new Forms.Form({
                    title: _("Name"),
                    controls: [
                        new Forms.Text("singleline", Forms.Text.bind(this.ref.editor, "name", "", undefined))
                            .placeholder(_("Unnamed"))
                            .autoFocus()
                            .autoSelect()
                    ]
                }),
                true
            )
            .locked();
    }

    private addStylesFeature(cards: Components.Cards.Cards): void {
        const checkboxes: {
            [id: string]: Forms.Checkbox;
        } = {};

        const groups: {
            [id: string]: Forms.Checkbox;
        } = {};

        const dependencies: {
            [id: string]: Forms.Control[];
        } = {};

        let buffer: {} | undefined;

        const onChange = (id: string) => {
            const checkbox = checkboxes[id] || groups[id];

            if (checkbox && dependencies[id]) {
                each(dependencies[id], (dependency: Forms.Control) => {
                    dependency.disabled(!this.ref.isPremium || !checkbox.isChecked);

                    if (!checkbox.isChecked) {
                        const group = findFirst(groups, (groupCheckbox: Forms.Checkbox) => groupCheckbox === dependency);

                        if (group) {
                            group.uncheck();
                        }
                    }
                });
            }

            this.changed();
        };

        if (this.ref.styles && this.ref.styles.length > 0) {
            const controls: (Forms.Control | Forms.Group)[] = [];

            each(filter(this.ref.styles, (styles: IStyles) => !styles.optional), (styles: IStyles) => {
                if (controls.length > 0) {
                    controls.push(new Forms.Spacer(), new Forms.Static(styles.title));
                }

                controls.push(
                    ...([] as Forms.Control[]).concat(
                        ...map(styles.styles, (style: IStyle) => {
                            const base = styles.base
                                ? (isObject(getAny(this.ref.settings.collector.style, styles.base)) &&
                                      getAny<{} | undefined>(this.ref.settings.collector.style, styles.base)) ||
                                  setAny(this.ref.settings.collector.style, styles.base, {})
                                : undefined;

                            const id = ((styles.base && `${styles.base}.`) || "") + style.name;
                            const control = SettingsComponent.addStyle(
                                style,
                                SettingsComponent.getValue(getAny(base || this.ref.settings.collector.style, style.name), style.default),
                                (newValue: string | boolean) => {
                                    if (!isString(newValue) || newValue) {
                                        setAny(base || this.ref.settings.collector.style, style.name, newValue);
                                    } else {
                                        destroy(base || this.ref.settings.collector.style, style.name);
                                    }

                                    onChange(id);
                                }
                            );

                            if (control[0] instanceof Forms.Checkbox) {
                                checkboxes[id] = control[0] as Forms.Checkbox;
                            }

                            if (style.dependsOn) {
                                const dependency = dependencies[style.dependsOn] || (dependencies[style.dependsOn] = []);

                                dependency.push(...control);
                            }

                            return control;
                        })
                    )
                );
            });

            const options = filter(this.ref.styles, (styles: IStyles) => styles.optional || false);

            if (options.length > 0) {
                const optionalControls: (Forms.Control | Forms.Group)[] = [];

                each(options, (styles: IStyles) => {
                    if (styles.styles && styles.styles.length > 0) {
                        const isEnabled = (styles.base && isObject(getAny(this.ref.settings.collector.style, styles.base))) || false;
                        let groupCheckbox: Forms.Checkbox;
                        let groupControl: Forms.Group;

                        optionalControls.push(
                            (groupCheckbox = new Forms.Checkbox(styles.title, isEnabled)
                                .description(styles.description || "")
                                .on((checkbox: Forms.Checkbox) => {
                                    groupControl.visible(checkbox.isChecked);

                                    if (!checkbox.isChecked && styles.base) {
                                        destroy(this.ref.settings.collector.style, styles.base);

                                        onChange(styles.base);
                                    }
                                }))
                        );

                        if (styles.base) {
                            groups[styles.base] = groupCheckbox;
                        }

                        if (styles.dependsOn) {
                            const dependency = dependencies[styles.dependsOn] || (dependencies[styles.dependsOn] = []);

                            dependency.push(groupCheckbox);
                        }

                        optionalControls.push(
                            (groupControl = new Forms.Group(
                                ([] as Forms.Control[]).concat(
                                    ...map(styles.styles, (style: IStyle) => {
                                        const id = ((styles.base && `${styles.base}.`) || "") + style.name;
                                        const control = SettingsComponent.addStyle(
                                            style,
                                            SettingsComponent.getValue(
                                                isEnabled && styles.base
                                                    ? getAny(getAny(this.ref.settings.collector.style, styles.base), style.name)
                                                    : undefined,
                                                style.default
                                            ),
                                            (newValue: string | boolean) => {
                                                if (groupCheckbox.isChecked && styles.base) {
                                                    if (!isString(newValue) || newValue) {
                                                        setAny(
                                                            getAny(this.ref.settings.collector.style, styles.base) ||
                                                                setAny(this.ref.settings.collector.style, styles.base, {}),
                                                            style.name,
                                                            newValue
                                                        );
                                                    } else {
                                                        destroy(getAny(this.ref.settings.collector.style, styles.base), style.name);
                                                    }

                                                    onChange(id);
                                                }
                                            }
                                        );

                                        each(control, (c: Forms.Control) => c.indent(32));

                                        if (control[0] instanceof Forms.Checkbox) {
                                            checkboxes[id] = control[0] as Forms.Checkbox;
                                        }

                                        if (style.dependsOn) {
                                            const dependency = dependencies[style.dependsOn] || (dependencies[style.dependsOn] = []);

                                            dependency.push(...control);
                                        }

                                        return control;
                                    })
                                )
                            ).visible(isEnabled))
                        );
                    }
                });

                if (optionalControls.length > 0) {
                    controls.push(new Forms.Static(_("Optional styles")), ...optionalControls);
                }
            }

            if (!this.ref.isPremium) {
                each(controls, (control: Forms.Control | Forms.Group) => control.disable());
            }

            cards.features
                .option(
                    _("Style"),
                    new Forms.Form({
                        title: _("Style"),
                        controls: [...(this.ref.isPremium ? [] : [SettingsComponent.addPremiumNotification()]), ...controls]
                    }),
                    this.settings.collector.style ? true : false
                )
                .onToggle((feature: Components.Cards.Feature) => {
                    if (feature.isEnabled) {
                        this.ref.settings.collector.style = this.ref.settings.collector.style || buffer || {};
                    } else {
                        buffer = this.ref.settings.collector.style;

                        this.ref.settings.collector.style = undefined;
                    }
                });
        }
    }

    private addRemoveBrandingFeature(cards: Components.Cards.Cards): void {
        cards.features.option(
            _("Branding"),
            new Forms.Form({
                title: _("Branding"),
                controls: [
                    ...(this.ref.isPremium ? [] : [SettingsComponent.addPremiumNotification(false)]),
                    new Forms.Checkbox(
                        _("Remove branding"),
                        Forms.Checkbox.bind(this.settings.collector, "removeBranding", undefined, this.ref.isPremium)
                    )
                        .on(() => this.changed())
                        .disabled(!this.ref.isPremium)
                        .description(_("Remove the Tripetto branding and links."))
                ]
            }),
            isBoolean(this.settings.collector.removeBranding)
        );
    }

    private addConfirmationFeature(cards: Components.Cards.Cards): void {
        const confirmationButtonGroup = new Forms.Group([
            new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.collector, "confirmationButtonLabel", undefined))
                .placeholder(_("Label"))
                .indent(32)
                .require()
                .on(() => this.changed()),
            new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.collector, "confirmationButtonUrl", undefined))
                .placeholder(_("https://"))
                .indent(32)
                .require()
                .on(() => this.changed())
        ])
            .disabled(!this.ref.isPremium)
            .visible(isString(this.ref.settings.collector.confirmationButtonLabel));

        cards.features.option(
            _("Confirmation"),
            new Forms.Form({
                title: _("Confirmation"),
                controls: [
                    ...(this.ref.isPremium ? [] : [SettingsComponent.addPremiumNotification()]),
                    new Forms.Static(_("This confirmation message is shown when a form is completed.")),
                    new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.collector, "confirmationTitle", undefined))
                        .placeholder(_("Title"))
                        .require()
                        .on(() => this.changed())
                        .disabled(!this.ref.isPremium),
                    new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.collector, "confirmationSubtitle", undefined))
                        .placeholder(_("Subtitle"))
                        .on(() => this.changed())
                        .disabled(!this.ref.isPremium),
                    new Forms.Text("multiline", Forms.Text.bind(this.ref.settings.collector, "confirmationText", undefined))
                        .placeholder(_("Finish message"))
                        .on(() => this.changed())
                        .disabled(!this.ref.isPremium),
                    new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.collector, "confirmationImage", undefined))
                        .placeholder(_("Optional image URL"))
                        .on(() => this.changed())
                        .disabled(!this.ref.isPremium),
                    new Forms.Checkbox(_("Show a button"), isString(this.ref.settings.collector.confirmationButtonLabel))
                        .on((button: Forms.Checkbox) => {
                            confirmationButtonGroup.visible(button.isChecked);
                        })
                        .disabled(!this.ref.isPremium),
                    confirmationButtonGroup
                ]
            }),
            isFilledString(this.settings.collector.confirmationTitle)
        );
    }

    private addEmailFeature(cards: Components.Cards.Cards): void {
        cards.features.option(
            _("Email"),
            new Forms.Form({
                title: _("Email notification"),
                controls: [
                    new Forms.Static(_("Receive an email notification when a new form entry is completed.")),
                    new Forms.Email(Forms.Email.bind(this.ref.settings.notifications, "email", undefined))
                        .on(() => this.changed())
                        .require(),
                    new Forms.Checkbox(
                        _("Include form data"),
                        Forms.Checkbox.bind(this.ref.settings.notifications, "includeFormDataInEmail", undefined)
                    )
                        .on(() => this.changed())
                        .description(_("Including safe download links for uploads"))
                ]
            }),
            isFilledString(this.settings.notifications.email)
        );
    }

    private addSlackFeature(cards: Components.Cards.Cards): void {
        cards.features.option(
            _("Slack"),
            new Forms.Form({
                title: _("Slack notification"),
                controls: [
                    ...(this.ref.isPremium ? [] : [SettingsComponent.addPremiumNotification()]),
                    new Forms.Static(_("Add an incoming webhook in your Slack team and paste the Webhook URL in the field below.")),
                    new Forms.Button(_("Create webhook in Slack"))
                        .url("https://slack.com/apps/A0F7XDUAZ-incoming-webhooks")
                        .width("auto")
                        .disabled(!this.ref.isPremium),
                    new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.notifications, "slack", undefined))
                        .require()
                        .on(() => {
                            if (this.ref.isPremium) {
                                this.changed();
                            }
                        })
                        .label("Webhook URL")
                        .disabled(!this.ref.isPremium)
                ]
            }),
            isFilledString(this.settings.notifications.slack)
        );
    }

    private addZapierFeature(cards: Components.Cards.Cards): void {
        cards.features.option(
            _("Zapier"),
            new Forms.Form({
                title: _("Zapier integration"),
                controls: [
                    ...(this.ref.isPremium ? [] : [SettingsComponent.addPremiumNotification()]),
                    new Forms.Static(
                        _(
                            "Create an incoming webhook zap in your Zapier account and paste the Webhook URL in the field below. Note: Please configure Zapier to handle the JSON result to your needs."
                        )
                    ),
                    new Forms.Button(_("Create new webhook zap in Zapier"))
                        .url("https://zapier.com/apps/webhook/integrations")
                        .width("auto")
                        .disabled(!this.ref.isPremium),
                    new Forms.Text("singleline", Forms.Text.bind(this.ref.settings.integrations, "zapier", undefined))
                        .require()
                        .on(() => {
                            if (this.ref.isPremium) {
                                this.changed();
                            }
                        })
                        .label("Webhook URL")
                        .disabled(!this.ref.isPremium)
                ]
            }),
            isFilledString(this.settings.integrations.zapier)
        );
    }

    onCards(cards: Components.Cards.Cards): void {
        cards.features.static(_("Form"));
        this.addNameFeature(cards);
        this.addStylesFeature(cards);
        this.addRemoveBrandingFeature(cards);
        this.addConfirmationFeature(cards);

        cards.features.static(_("Notifications"));
        this.addEmailFeature(cards);
        this.addSlackFeature(cards);

        cards.features.static(_("Integrations"));
        this.addZapierFeature(cards);
    }
}
