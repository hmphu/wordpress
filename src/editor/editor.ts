import { Editor, IDefinition, IEditorStyle, IEditorEditEvent, Layers } from "tripetto";
import { SettingsComponent, ISettings, TStyles } from "./settings";

function openSettings(editor: Editor, settings: ISettings, isPremium: boolean, styles: TStyles | undefined): void {
    const component = editor.createPanel((layer: Layers.Layer, editor: Editor, style: IEditorStyle) =>
        SettingsComponent.open(layer, editor, style, styles, settings, isPremium)
    );

    window.parent.postMessage(
        {
            type: "settings:open"
        },
        window.location.origin
    );

    component.whenChanged = (changedSettings: ISettings) =>
        window.parent.postMessage(
            {
                type: "settings",
                settings: changedSettings
            },
            window.location.origin
        );

    component.whenClosed = () =>
        window.parent.postMessage(
            {
                type: "settings:close"
            },
            window.location.origin
        );
}

export async function editor(
    pkg: {
        STYLES: TStyles | undefined;
    },
    props: ISettings & { readonly definition?: IDefinition; readonly baseUrl: string; readonly isPremium: boolean }
): Promise<void> {
    const editor = Editor.open(props.definition, {
        fonts: `${props.baseUrl}/fonts/`,
        disableLogo: true,
        disableSaveButton: true,
        disableRestoreButton: true,
        disableClearButton: true,
        disableEditButton: true,
        disableCloseButton: true,
        disableTutorialButton: true,
        disableOpenCloseAnimation: true,
        supportURL: false,
        zoom: "fit-horizontal"
    });

    editor.hook("OnReady", "framed", () =>
        window.parent.postMessage(
            {
                type: "ready",
                definition: editor.definition,
                name: editor.name
            },
            window.location.origin
        )
    );

    editor.hook("OnRename", "framed", () =>
        window.parent.postMessage(
            {
                type: "rename",
                definition: editor.definition,
                name: editor.name
            },
            window.location.origin
        )
    );

    editor.hook("OnChange", "framed", () =>
        window.parent.postMessage(
            {
                type: "definition",
                definition: editor.definition,
                name: editor.name
            },
            window.location.origin
        )
    );

    editor.hook("OnEdit", "framed", (event: IEditorEditEvent) => {
        if (event.type === "node" && event.id) {
            window.parent.postMessage(
                {
                    type: "requestPreview",
                    id: event.id
                },
                window.location.origin
            );
        }
    });

    window.addEventListener("message", (e: MessageEvent) => {
        switch ((e.data && e.data.type) || e.data) {
            case "settings":
                openSettings(editor, props, props.isPremium, pkg.STYLES);
                break;
            case "tutorial":
                editor.tutorial();
                break;
            case "editRequest":
                editor.edit(e.data.id);
                break;
        }
    });

    window.addEventListener("resize", () => editor.resize());
    window.addEventListener("orientationchange", () => editor.resize());
}
