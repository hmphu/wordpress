<?php
function uninstall()
{
    global $wpdb;

    $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_forms");
    $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_entries");
    $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_attachments");

    delete_option("tripetto_version");
}
?>
