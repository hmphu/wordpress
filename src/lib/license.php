<?php

class License
{
    /**
     * Returns whether a form has premium features.
     *
     * @param array $formId The id of the form.
     */
    static function hasPremiumFeatures($formId)
    {
        if (License::isInPremiumMode()) {
            return true;
        }

        License::validateMaximumPremiumForms();

        global $wpdb;

        $table = $wpdb->prefix . "tripetto_forms";
        $premium = $wpdb->get_var(
            $wpdb->prepare("SELECT premium from $table where id=%d", $formId)
        );

        if (!is_null($premium) && $premium > 0) {
            return true;
        }

        return false;
    }

    /**
     * Returns whether the plugin is in premium mode.
     */
    static function isInPremiumMode()
    {
        if (t_freemius()->can_use_premium_code__premium_only()) {
            return true;
        }

        return false;
    }

    /**
     * Marks a form as premium form.
     */
    static function markAsPremium($formId)
    {
        if (!is_null($formId)) {
            global $wpdb;
            $table = $wpdb->prefix . "tripetto_forms";

            $query = "UPDATE $table SET premium=0";
            $wpdb->query($query);

            $query = "UPDATE $table SET premium=1 WHERE id = $formId";
            $wpdb->query($query);
        }
    }

    /**
     * Validates the maximum number of premium forms and resets all premium
     * forms if there has been tampered with the number of premium forms.
     */
    static function validateMaximumPremiumForms()
    {
        if (!License::isInPremiumMode()) {
            $premiumFormCount = License::premiumFormCount();

            if ($premiumFormCount == 0) {
                $premiumFormCandidate = License::premiumFormCandidate();
                License::markAsPremium($premiumFormCandidate);
            }
            if ($premiumFormCount > 1) {
                License::resetAllPremiumForms();
            }
        }
    }

    /**
     * Returns the candidate form to mark as premium.
     */
    private static function premiumFormCandidate()
    {
        global $wpdb;
        $table = $wpdb->prefix . "tripetto_forms";

        return $wpdb->get_var("SELECT MIN(id) FROM $table");
    }

    /**
     * Returns the number of premium forms.
     */
    static function premiumFormCount()
    {
        global $wpdb;
        $table = $wpdb->prefix . "tripetto_forms";

        return $wpdb->get_var("SELECT COUNT(id) FROM $table WHERE premium > 0");
    }

    /**
     * Resets premium features for all forms.
     */
    static function resetAllPremiumForms()
    {
        global $wpdb;
        $table = $wpdb->prefix . "tripetto_forms";

        $premiumFormIds = $wpdb->get_col(
            "SELECT id FROM $table WHERE premium > 0"
        );

        License::resetPremiumFormFeatures($premiumFormIds);
    }

    /**
     * Resets the premium features for an array of forms.
     */
    private static function resetPremiumFormFeatures($formIds)
    {
        if (is_array($formIds) && count($formIds) > 0) {
            $formIds = implode(',', $formIds);

            global $wpdb;
            $table = $wpdb->prefix . "tripetto_forms";

            $query = "UPDATE $table SET premium=0,collector_remove_branding=-1,collector_style='',confirmation_title='',confirmation_subtitle='',confirmation_text='',confirmation_image='',confirmation_button_label='',confirmation_button_url='',notification_slack='',integration_zapier='' WHERE id IN ($formIds)";
            $wpdb->query($query);
        }
    }

    static function upgrade()
    {
        if (db_need_upgrade()) {
            License::validateMaximumPremiumForms();
        }
    }
}
?>
