<?php

class Entry
{
    /**
     * Delete an array of entries.
     *
     * @param array $ids The ids of the entries.
     */
    static function delete($ids)
    {
        if (!empty($ids)) {
            if (is_array($ids)) {
                $ids = implode(',', $ids);
            }

            global $wpdb;
            $table_name = $wpdb->prefix . 'tripetto_attachments';

            $attachments = $wpdb->get_col(
                "SELECT id FROM $table_name WHERE entry_id IN ($ids)"
            );
            foreach ($attachments as $attachment) {
                Attachment::delete($attachment, true);
            }

            $table_name = $wpdb->prefix . 'tripetto_entries';
            $wpdb->query("DELETE FROM $table_name WHERE id IN ($ids)");
        }
    }
}
?>
