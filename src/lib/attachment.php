<?php

class Attachment
{
    static function formatFilename($filename, $formId, $attachmentId)
    {
        return $formId . '_' . $attachmentId . '_' . $filename;
    }

    /**
     * Delete an attachment.
     *
     * @param int $attachmentId The id of the attachment.
     * @param boolean $confirmed True when the attachment is connected with an entry.
     */
    static function delete($attachmentId, $confirmed)
    {
        global $wpdb;

        $attachments_table_name = $wpdb->prefix . "tripetto_attachments";
        $query = sprintf(
            "SELECT * FROM $attachments_table_name WHERE id=%d AND entry_id IS %s NULL",
            $attachmentId,
            $confirmed ? 'NOT' : ''
        );
        $attachment = $wpdb->get_row($query);

        if (!is_null($attachment)) {
            // Delete the record from the database.
            $wpdb->delete($attachments_table_name, array(
                'id' => $attachmentId
            ));

            // Delete the file from disk.
            $path =
                $attachment->path .
                '/' .
                Attachment::formatFilename(
                    $attachment->name,
                    $attachment->form_id,
                    $attachment->id
                );
            wp_delete_file($path);
        }
    }
}
?>
