<?php

class Helpers
{
    /** Retrieves the root plugin URL (without trailing `/`). */
    static function plugin_url()
    {
        return rtrim(plugin_dir_url(dirname(__FILE__)), "/");
    }
}
?>
