<?php

class Template
{
    static function render($file, $replacements)
    {
        $file = dirname(__FILE__) . '/..' . $file;

        if (!file_exists($file)) {
            return false;
        }

        ob_start();
        include $file;
        $body = ob_get_contents();
        ob_end_clean();

        foreach ($replacements as $key => $value) {
            $body = str_replace('{{' . $key . '}}', $value, $body);
        }

        return $body;
    }

    static function renderEmailFooterTemplate($recipient, $pluginUrl)
    {
        $footerReplacements = array(
            'recipient' => $recipient,
            'url' => $pluginUrl
        );
        return Template::render(
            '/views/emails/partials/footer.php',
            $footerReplacements
        );
    }
}
?>
