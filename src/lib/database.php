<?php
global $tripetto_version;

$tripetto_version = '{{ VERSION }}';

function db_need_upgrade()
{
    global $tripetto_version;

    $installed_ver = get_option("tripetto_version");

    if ($installed_ver != $tripetto_version) {
        return true;
    }

    return false;
}

function db_install()
{
    global $tripetto_version;

    add_option("tripetto_version", $tripetto_version);
}

function db_upgrade()
{
    global $tripetto_version;

    update_option("tripetto_version", $tripetto_version);
}

function db_verify()
{
    global $wpdb;

    if (!empty($wpdb->last_error)) {
        die(
            "There was a problem activating Tripetto. Please report the following error to support@tripetto.com: " .
                $wpdb->last_error
        );
    }
}
?>
