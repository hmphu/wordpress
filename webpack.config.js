const webpack = require("webpack");
const webpackCopy = require("copy-webpack-plugin");
const webpackTerser = require("terser-webpack-plugin");
const webpackReplace = require("replace-in-file-webpack-plugin");
const webpackExtract = require("mini-css-extract-plugin");
const path = require("path");
const package = require("./package.json");
const production = process.argv.includes("production");

require("dotenv").config();

const config = type => {
    return {
        entry: `./src/${type}/${type}.ts`,
        output: {
            filename: `wp-tripetto-${type}.js`,
            path: path.resolve(__dirname, "dist", "scripts"),
            libraryTarget: "umd",
            library: "WPTripetto",
            umdNamedDefine: true
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loader: "ts-loader",
                    options: {
                        configFile: "tsconfig.json",
                        compilerOptions: {
                            noEmit: false
                        }
                    }
                },
                {
                    test: /\.(scss)$/,
                    use: [
                        {
                            loader: webpackExtract.loader
                        },
                        {
                            loader: "css-loader"
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: function() {
                                    return [require("precss"), require("autoprefixer")];
                                }
                            }
                        },
                        {
                            loader: "sass-loader"
                        }
                    ]
                },
                {
                    test: /\.svg$/,
                    use: ["url-loader"]
                },
                {
                    test: /\.(woff|woff2|eot|ttf)$/,
                    use: [
                        {
                            loader: "url-loader"
                        }
                    ]
                }
            ]
        },
        resolve: {
            extensions: [".ts", ".js"]
        },
        externals: {
            tripetto: "Tripetto",
            "tripetto-collector": "TripettoCollector",
            "tripetto-collector-rolling": "TripettoCollectorRolling",
            "tripetto-collector-standard-bootstrap": "TripettoCollectorStandardBootstrap"
        },
        performance: {
            hints: false
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        output: {
                            comments: false
                        }
                    },
                    extractComments: false
                }),
                new webpack.BannerPlugin(`${package.title} ${package.version}`)
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                Promise: "es6-promise-promise"
            }),
            new webpackExtract({
                filename: `../css/wp-tripetto-${type}.css`
            }),
            ...((type === "admin" && [
                new webpackCopy([
                    {
                        from: "./src/**/*.+(php|txt)",
                        to: "..",
                        transformPath(targetPath) {
                            return targetPath.replace("src\\", "").replace("src/", "");
                        }
                    },
                    {
                        from: "./static",
                        to: ".."
                    },
                    {
                        from: "./src/languages",
                        to: "../languages"
                    },
                    {
                        from: "./dependencies/freemius",
                        to: "../freemius",
                        ignore: ["*.yml", ".github/*", ".git*", "gulpfile.js", "composer.json", "package.json"]
                    },
                    {
                        from: "node_modules/tripetto/fonts/",
                        to: "../fonts/"
                    },
                    {
                        from: "node_modules/tripetto/runtime/tripetto-umd.js",
                        to: "../vendors/tripetto-editor.js"
                    },
                    {
                        from: "node_modules/tripetto-collector/dist/tripetto-collector-es5.js",
                        to: "../vendors/tripetto-collector.js"
                    },
                    {
                        from: "node_modules/tripetto-collector-rolling/collector/umd/index.js",
                        to: "../vendors/tripetto-collector-rolling.js"
                    },
                    {
                        from: "node_modules/tripetto-collector-rolling/editor/umd/index.js",
                        to: "../vendors/tripetto-editor-rolling.js"
                    },
                    {
                        from: "node_modules/tripetto-collector-standard-bootstrap/collector/umd/index.js",
                        to: "../vendors/tripetto-collector-standard-bootstrap.js"
                    },
                    {
                        from: "node_modules/tripetto-collector-standard-bootstrap/editor/umd/index.js",
                        to: "../vendors/tripetto-editor-standard-bootstrap.js"
                    }
                ]),
                new webpackReplace([
                    {
                        dir: "dist",
                        files: ["plugin.php", "readme.txt", "lib/database.php"],
                        rules: [
                            {
                                search: "{{ NAME }}",
                                replace: "Tripetto"
                            },
                            {
                                search: "{{ TITLE }}",
                                replace: package.title
                            },
                            {
                                search: "{{ DESCRIPTION }}",
                                replace: package.description
                            },
                            {
                                search: /\{\{\sVERSION\s\}\}/gi,
                                replace: package.version
                            },
                            {
                                search: "{{ HOMEPAGE }}",
                                replace: package.homepage
                            },
                            {
                                search: "{{ KEYWORDS }}",
                                replace: (package.keywords || []).join(", ")
                            },
                            {
                                search: "'secret_key' => ''",
                                replace: `'secret_key' => '${process.env.FREEMIUS_SECRET_KEY || ""}'`
                            }
                        ]
                    }
                ])
            ]) ||
                [])
        ]
    };
};

module.exports = () => {
    return [config("admin"), config("editor"), config("preview"), config("collector")];
};
