\+ New feature
\* Change or bugfix
\- Deprecated or removed feature

08-10-2019:
  * Fixed a bug in the hidden field block

10-09-2019:
  * Fixed a bug in the markdown parser
  * Fixed a bug where the collector sometimes didn't load on certain pages
  * Fixed a bug where Wordfence mistakenly thought a script was malicious

04-09-2019:
  * Fixed a bug where the plugin would not work with MySQL version 5.5 (or older)

28-08-2019:
  * Fixed a bug where the wrong plugin directory was used

27-08-2019:
  * Removed jQuery dependency

21-08-2019:
  * Fixed issue #77 (https://gitlab.com/tripetto/wordpress/issues/77)
  * Fixed a bug in the file upload handling

06-08-2019:
  + Added a brand new collector with a standard UI based on Bootstrap (for those who want to create a more traditional form)
  + Added a toggle to the header bar in the edit screen to switch between the Rolling UI or the new Standard UI
  + Added a toggle to the header bar in the edit screen to switch between edit mode and test mode (in edit mode all blocks are displayed in the preview pane, test mode runs the form with all logic enabled so you can test it)
  + Added edit buttons in the preview pane to quickly open the properties of a block
  + Automatically scroll blocks into view when they are edited
  * Improved the header bar in the editor screen to incorporate all the new options (moved the device toggle buttons to a separate dropdown)
  * Improved the shortcode so it is not necessarry anymore to specify a height for the form (you still can specify a fixed height if you want)
  * Fixed a bug that caused problems when 2 or more forms were placed on the same page
  * Installed the latest and greatest version of the Rolling UI collector

31-07-2019:
  * Fixed a bug with the sender's name in email notifications ([#76](https://gitlab.com/tripetto/wordpress/issues/76))

25-07-2019:
  + Added an option to generate random values with the hidden field block
  + Added the option to align the first block of the collector at the top instead of the center of the screen
  * Moved some collector options (navigation bar, enumerators, etc.) to the styles section of the settings panel
  * Upgraded to a new version of the rolling collector with improved performance and also some bug fixes

17-07-2019:
  + Added hidden field block (this allows the use of hidden fields in forms and also logic based on the value of those hidden fields)

16-07-2019:
  + Added the option to include the form data in confirmation mails
  + Added the "Send an email"-block to send emails from within forms
  * Fixed bug in the editor position when WP menu is collapsed
  * Fixed a bug in keyboard navigation in the collector
  * Fixed a bug where focus sometimes was set on the wrong field in the settings panel of the editor

10-07-2019:
  + Added multiple checkboxes block
  + Added radiobuttons block
  + Added the possibility to hide the title of a block
  * Fixed problem with the style of the checkbox block
  * Renamed `Next` button to `Ok` button

09-07-2019:
  + Added single checkbox block
  + Added ability to change the labels of the `Next` and `Complete` buttons in the form style panel
  + Added uninstall script to remove the data tables when the plugin is deleted
  * Automatically make blocks required when the required feature is enabled (this saves an additional click by the user)
  * Fixed a bug in the positioning of the empty message of the collector

02-07-2019:
  + Confirmation dialogs when a form or entry is about to be deleted
  * Fixed a bug in the Zapier-integration
  * Unlocked all premium functionality for one form without the need of having a paid license

14-06-2019:
  + Initial release in WordPress Plugin Directory
